from geometry import *


class Node(object):
    def __init__(self, site, point, nil):
        self.nil = nil
        self.left = nil
        self.right = nil
        self.parent = nil
        self.redColor = False
        self.left_neighbor = nil
        self.right_neighbor = nil

        # Data
        # - The site defines a parabola
        # - The point identifies the proper piece of the parabola
        self.site = site
        self.p = point

        # Used for comparison
        self.pos_epsilon = None

        # pointer to event queue circle event
        self.circle_event = None

        # DCEL
        self.left_edge = None
        self.right_edge = None


    def parabola(self, sweepline_y):
        return Parabola.from_site_and_sweepline(self.site, sweepline_y)

    def interval(self, sweepline_y, lb=-float("inf"), ub=float("inf")):
        s1 = self.left_neighbor.site
        s2 = self.site
        s3 = self.right_neighbor.site

        if s2 is None:
            print "node.interval: self.site is None. Something is wrong"
            return

        p2 = Parabola.from_site_and_sweepline(s2, sweepline_y)

        if s1 is not None:
            p1 = Parabola.from_site_and_sweepline(s1, sweepline_y)
            if p1.equals(p2):
                print "Parabola and left neighbor are equal (%s, %s). No unique intersection point." % (s1, p1)
                return
            i1 = p1.intersects(p2)
            if i1 is None:
                print "No intersection of parabola %s and %s (sites: %s and %s)" % (p1,p2,s1,s2)
                return
            if isinstance(i1, Point): # Only one solution
                x1 = i1.x
            else: # Two solutions, take the left
                x1 = i1[0].x
        else:
            x1 = lb

        if s3 is not None:
            p3 = Parabola.from_site_and_sweepline(s3, sweepline_y)
            if p2.equals(p3):
                print "Parabola and right neighbor are equal (%s, %s). No unique intersection point." % (s3, p3)
            i2 = p2.intersects(p3)
            if i2 is None:
                print "No intersection of parabola %s and %s (sites: %s and %s)" % (p1,p2,s1,s2)
                return
            if isinstance(i2, Point): # Only one solution
                x2 = i2.x
            else: # Two solutions, take the left
                x2 = i2[0].x
        else:
            x2 = ub

        return (x1, x2)

    def inOrderTreeWalk(self, func):
        if self.left is not self.nil:
            self.left.inOrderTreeWalk(func)

        func(self)

        if self.right is not self.nil:
            self.right.inOrderTreeWalk(func)

    def __str__(self):
        if self is self.nil:
            return "<Nil>"
        if self.left is self.nil and self.right is self.nil:
            label = "Leaf"
        else:
            label = "Node"
        color = "red" if self.redColor else "black"
        return "<%s %s, %s>" % (label, self.site, color)

class Rbt(object):
    def __init__(self, create_node=Node, compare_nodes=cmp):
        self.nil = create_node(None, None, None)
        self.nil.nil = self.nil
        self.nil.left = self.nil
        self.nil.right = self.nil
        self.nil.parent = self.nil
        self.root = self.nil
        self.create_node = create_node
        self.compare_nodes = compare_nodes
    

    def insert(self, site, guiding_point, pos_epsilon=None, left_edge=None, right_edge=None):
        node = self.create_node(site, guiding_point, self.nil)
        node.pos_epsilon = pos_epsilon
        node.left_edge = left_edge
        node.right_edge = right_edge
        self.rbInsert(node)

    def rbInsert(self, z):
        y = self.nil
        x = self.root
        while x is not self.nil:
            y = x
            comparison = self.compare_nodes(z, x)
            if comparison < 0: # z.key < x.key
                x = x.left
            elif comparison == 0: # z.key = x.key
                if z.pos_epsilon == True:
                    if x.left is self.nil:
                        x = x.left
                    else:
                        y = self.rbGetMax(x.left)
                        x = y.right
                elif z.pos_epsilon == False:
                    if x.right is self.nil:
                        x = x.right
                    else:
                        y = self.rbGetMin(x.right)
                        x = y.left
                else:
                    print "z.pos_epsilon == None"
            else: # comparison > 0 # z.key > x.key
                x = x.right
        z.parent = y
        if y is self.nil:
            self.root = z
        elif self.compare_nodes(z, y) < 0: # z.key < y.key
            y.left = z
        elif self.compare_nodes(z, y) == 0:
            if z.pos_epsilon == True:
                y.left = z
            elif z.pos_epsilon == False:
                y.right = z
            else:
                print "z.position == None. What should I do?"
        else:
            y.right = z
        z.left = self.nil
        z.right = self.nil
        #assign neighbors of z and make z neighbors neighbor
        if z.parent.left is z:
            z.right_neighbor = z.parent
            z.left_neighbor = z.parent.left_neighbor
            z.parent.left_neighbor = z
            if z.left_neighbor is not self.nil:         #if z is not leftmost node
                z.left_neighbor.right_neighbor = z

        elif z.parent.right is z:
            z.left_neighbor = z.parent
            z.right_neighbor = z.parent.right_neighbor
            z.parent.right_neighbor = z
            if z.right_neighbor is not self.nil:        #if z is not rightmost node
                z.right_neighbor.left_neighbor = z
            
        z.redColor = True
        self.rbInsertFixup(z)

    def rbInsertFixup(self, z):
        while z.parent.redColor == True:
            if z.parent is z.parent.parent.left:
                y = z.parent.parent.right
                if y.redColor == True:
                    # case 1
                    z.parent.redColor = False
                    y.redColor = False
                    z.parent.parent.redColor = True
                    z = z.parent.parent 
                else: 
                    if z is z.parent.right:
                        # case 2
                        z = z.parent    
                        self.leftRotate(z)
                    # case 3
                    z.parent.redColor = False
                    z.parent.parent.redColor = True
                    self.rightRotate(z.parent.parent)
            # Whole if thing mirrored
            elif z.parent is z.parent.parent.right:
                y = z.parent.parent.left
                if y.redColor == True:
                    #case 1
                    z.parent.redColor = False
                    y.redColor = False
                    z.parent.parent.redColor = True
                    z = z.parent.parent
                else:
                    if z is z.parent.left:
                        #case 2
                        z = z.parent
                        self.rightRotate(z)
                    z.parent.redColor = False
                    z.parent.parent.redColor = True
                    self.leftRotate(z.parent.parent)
        self.root.redColor = False


    def leftRotate(self, x):
        y = x.right
        x.right = y.left
        if y.left is not self.nil:
            y.left.parent = x
        y.parent = x.parent
        if x.parent is self.nil:
            self.root = y
        elif x is x.parent.left:
            x.parent.left = y
        else: 
            x.parent.right = y
        y.left = x
        x.parent = y


    def rightRotate(self, x):
        y = x.left
        x.left = y.right
        if y.right is not self.nil:
            y.right.parent = x
        y.parent = x.parent
        if x.parent is self.nil:
            self.root = y
        elif x is x.parent.right:
            x.parent.right = y
        else:
            x.parent.left = y
        y.right = x
        x.parent = y                            


    def rbTransplant(self, u, v):
        if u.parent is self.nil:
            self.root = v
        elif u is u.parent.left:
            u.parent.left = v
        else:
            u.parent.right = v
        v.parent = u.parent

    def find(self, site, guiding_point, return_parent=False):
        # Create a node around the site
        # such that we can use it for comparison
        tmp = self.create_node(site, guiding_point, self.nil)

        # Start at the root
        x = self.root
        p = self.nil
        while x is not self.nil:
            p = x
            comparison = self.compare_nodes(tmp, x)
            if comparison == 0:
                return x if not return_parent else x.parent
            elif comparison < 0:
                x = x.left
            else:
                x = x.right
        return None if not return_parent else p


    def delete(self, site, guiding_point):
        found = self.find(site, guiding_point)
        if found is not None:
            self.rbDelete(found)
        else:
            # "Deleting site %s, but it is not present in the tree" % site
            return


    def rbDelete(self, z):
        #gonna try to fix neighbors in beginning
        if z.left_neighbor is self.nil:
            z.right_neighbor.left_neighbor = self.nil
        elif z.right_neighbor is self.nil:
            z.left_neighbor.right_neighbor = self.nil
        else:
            z.left_neighbor.right_neighbor = z.right_neighbor
            z.right_neighbor.left_neighbor = z.left_neighbor


        y = z
        yColorTmp = y.redColor
        if z.left is self.nil:
            x = z.right
            self.rbTransplant(z, z.right)
        elif z.right is self.nil:
            x = z.left
            self.rbTransplant(z, z.left)
        else:
            y = self.rbGetMin(z.right)
            yColorTmp = y.redColor
            x = y.right
            if y.parent is z:
                x.parent = y
            else:
                self.rbTransplant(y, y.right)
                y.right = z.right
                y.right.parent = y
            self.rbTransplant(z,y)
            y.left = z.left
            y.left.parent = y
            y.redColor = z.redColor
        if yColorTmp == False:
            self.rbDeleteFixup(x)


    def rbDeleteFixup(self, x):
        while x is not self.root and x.redColor == False:
            if x is x.parent.left:
                w = x.parent.right
                if w.redColor == True:
                    #case 1
                    w.redColor = False
                    x.parent.redColor = True
                    self.leftRotate(x.parent)
                    w = x.parent.right
                if w.left.redColor == False and w.right.redColor == False:
                    #case 2
                    w.redColor = True
                    x = x.parent
                else:
                    if w.right.redColor == False:
                        #case 3
                        w.left.redColor = False
                        w.redColor = True
                        self.rightRotate(w)
                        w = x.parent.right
                    #case 4 
                    w.redColor = x.parent.redColor
                    x.parent.redColor = False
                    w.right.redColor = False
                    self.leftRotate(x.parent)
                    x = self.root
            else:
                w = x.parent.left
                if w.redColor == True:
                    #case 1
                    w.redColor = False
                    x.parent.redColor = True
                    self.rightRotate(x.parent)
                    w = x.parent.left
                if w.right.redColor == False and w.left.redColor == False:
                    #case 2
                    w.redColor = True
                    x = x.parent
                else:
                    if w.left.redColor == False:
                        #case 3
                        w.right.redColor = False
                        w.redColor = True
                        self.leftRotate(w)
                        w = x.parent.left
                    #case 4
                    w.redColor = x.parent.redColor
                    x.parent.redColor = False
                    w.left.redColor = False
                    self.rightRotate(x.parent)
                    x = self.root
        x.redColor = False      


    def rbGetMin(self, starting_at = None):
        if starting_at is None:
            x = self.root
        else:
            x = starting_at

        while x.left is not self.nil:
            x = x.left
        return x

    def rbGetMax(self, starting_at = None):
        if starting_at is None:
            x = self.root
        else:
            x = starting_at

        while x.right is not self.nil:
            x = x.right
        return x

    def is_empty(self):
        return self.root is self.nil

    def inOrderTreeWalk(self, func):
        if self.root is not self.nil:
            self.root.inOrderTreeWalk(func)

    def __str__(self):
        return "Root: %s" % self.root

    def toDot(self):
        def node_to_dot(node):
            color_str = "red" if node.redColor else "black"
            label = "%s\n%s" % (str(node.site), str(node.p)) if node is not self.nil else "<Nil>"
            return "  \"%s\" [label=\"%s\",color=%s]; \n" % (id(node), label, color_str)

        def link_to_dot(parent, child, dir_str="", style="", color="", fillcolor=""):
            return "  \"%s\" -> \"%s\" [label=\"%s\",style=\"%s\",color=\"%s\",fillcolor=\"%s\"]; \n" % (id(parent), id(child), dir_str, style, color, fillcolor)

        node_queue = [self.root]
        node_str = ""
        link_str = ""

        while len(node_queue) > 0:
            parent = node_queue.pop()
            node_str += node_to_dot(parent)

            # Added neighbors to graph
            if parent.right_neighbor is not self.nil:
                link_str += link_to_dot(parent, parent.right_neighbor, style="dashed", color="#CCEECC")
            if parent.left_neighbor is not self.nil:
                link_str += link_to_dot(parent, parent.left_neighbor, style="dashed", color="#EECCCC")

            # Add children to graph
            if parent.right is not self.nil:
                link_str += link_to_dot(parent, parent.right, "right")
                node_queue.append(parent.right)
            if parent.left is not self.nil:
                link_str += link_to_dot(parent, parent.left, "left")
                node_queue.append(parent.left)

        return "digraph {\n%s\n%s\n}" % (node_str, link_str)
