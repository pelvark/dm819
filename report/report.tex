\documentclass[12pt, a4paper]{article}

% Font & text
\usepackage{times}
\usepackage[utf8]{inputenc} 
\setlength{\parindent}{0in}
\usepackage{hyperref}
\hypersetup{%
    pdfborder = {0 0 0}
}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}

\newcommand{\im}{\mathrm{Im}}
\newcommand{\p}[1]{\left( #1 \right)}
\newcommand{\lr}[3]{\left#1 #3 \right#2}
\newcommand{\st}{\mathrm{s.t.}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\B}{\mathbb{B}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\atan}{\mathrm{atan}}
\newcommand{\atantwo}{\mathrm{atan2}}
\newcommand{\Oh}{\mathcal{O}}
\newcommand{\lineseg}[1]{\overline{#1}}

% Source code
\usepackage{listings}
\usepackage{color}

\newcommand{\addcode}[1]{
\subsection*{#1}
\lstinputlisting[language=python, xleftmargin=-5em,xrightmargin=-5em]{../#1}
}

\lstset{
    language=Python,
    showstringspaces=false,
    breaklines=true,
    breakatwhitespace=false,
    formfeed=\newpage,
    tabsize=4,
    numbers=left,
    numbersep=5pt,
    numberstyle=\tiny\color{gray},
    commentstyle=\itshape\color{gray},
    keywordstyle=\bfseries\color{blue!40!black},
    identifierstyle=\color{black},
    stringstyle=\color{green!50!black},
    basicstyle=\ttfamily,
    morekeywords={lambda}
}


% Algorithms
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}

\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother


% Proofs
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

\renewcommand\qedsymbol{$\blacksquare$}


% Page, Header & Footer
%\usepackage{geometry}
\pagestyle{empty}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\cfoot{}
\rfoot{\thepage\ of \pageref{LastPage}}
\rhead{}

% Images
\usepackage{tikz}

\usepackage{titling}
\date{4th December 2015}
\author{Peter Severin Rasmussen \and Jon Kingo Christensen}
\def\identification{Peter Severin Rasmussen, 250394, peras13@student.sdu.dk \\ Jon Kingo Christensen, 180491, jonch10@student.sdu.dk}
\def\courseCode{DM819}
\def\courseTitle{Computational Geometry}
\def\lectorLabel{Teacher:}
\def\lector{Kim Skak Larsen}
\def\universityName{Syddansk Universitet}
\def\papertitle{Assignment 2}
\def\papersubtitle{\courseTitle}
\title{\papertitle{} - \papersubtitle{}}




% ============ %
\begin{document}
%\maketitle

\begin{tabular}{@{}l}
\universityName{} \\
\lectorLabel{} \lector{}
\end{tabular}
\hfill
\begin{tabular}{r@{}}
\courseTitle{} (\courseCode{}) \\
\thedate{}
\end{tabular}

\bigskip

% Frontpage Title
\begin{center}
    \Huge{\papertitle{}} \\
    \Large{\papersubtitle{}}
\end{center}
\normalsize{}

\begin{center}
    \identification{}
\end{center}

%\bigskip

%\newpage

%\restoregeometry{}



\section*{Specification}
In the following paper a plane sweep algorithm will be implemented to create a voronoi diagram, and some graphical representation of said voronoi diagram.

\medskip

More formally, the algorithm is given a list of $n$ points, which will from here on be refered to as sites.
Then a voronoi diagram represented by a doubly connected edge list is created. The output of the program will be a list of lines that make up the edges of the voronoi diagram, for the sake of easy understanding.

\subsection*{Assumptions for general position}

The algorithm will only handle general position under the following assumptions:

\begin{itemize}
    \item No coinciding sites
    \item No three sites are colinear
    \item No two sites with same largest y-coordinate
\end{itemize}


%\subsection*{Special cases} 





\section*{Design}
The problem is solved by using a plane sweep algorithm with all the sites, sorted on the y-axis, as the initial event queue.

The algorithm works by having a "beachline" of arcs of parabolas under the sites. Specifically any point within a beachline of a site, is closer to that site than any other site. This property is also true for any unencountered sites, because any point on the beachline of the parabola, has the same distance to the parabolas site, as to the sweepline. 
Whenever three such arcs intersect, it must mean that the intersecting points distance to all three of the sites corresponding to the beachlines is the same. This means that this intersection is a vertex in the voronoi diagram. Any such intersection is calculated during the plane sweep algorithm, either when sites are inserted in a site event, or when arcs are removed due to it being swallowed up by such an intersection. these intersections are called circle events.

Throughout the algorithm all the possible vertices of the resulting voronoi diagram is calculated and is to be used as event points themselves. Thus a dynamic datastructure should be used for the event queue. This implementation has used a maximum heap for this purpose.

The algorithm uses parabolas and their intersections to calculate where the vertices in the voronoi diagram is. Arcs of these parabolas must be kept in a datastructure sorted on the x coordinate. And there must be easy access to neighbors of arcs. This implementation has used a red black tree with a linked list of neighbors to keep track of the status of these arcs.


\begin{algorithm}
    \caption{Voronoi Diagram}
    \label{algorithm_1}
    \begin{algorithmic}[1]
        \Procedure{VoronoiDiagram}{$P$}
            \State $\mathcal{Q} \gets \text{Create event queue of } P$
            \State $\mathcal{T} \gets \text{Create initial status}$
            \State $\mathcal{D} \gets \text{Empty DCEL}$
            \While{$\mathcal{Q}$ not empty}
                \State $e \gets \text{Extract max of } \mathcal{Q}$
                \If {$e$ is site event}
                    \State \Call{HandleSiteEvent}{$e$}
                \Else
                    \State \Call{HandleCircleEvent}{$e$}
                \EndIf
            \EndWhile
            \State Remaining nodes of $\mathcal{T}$ represent the infinite half-edges 
            \State These must be inserted in $\mathcal{D}$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}


The algorithm works as seen in the pseudocode in Algorithm~\ref{algorithm_1}.

\paragraph{Site events}
If an event is a site event, a new parabola must be inserted. Since some arc of a parabola must be above this point, unless it is the first point encountered. The above arc must be split in two smaller arcs, one on each side of the new point.
%This should be shown with a picture
After such an insertion the circle events of the involving arcs must be calculated and added to the event queue.
An edge in the voronoi diagram is created at this point, since the must be an edge strictly between the new site and the site corresponding to the splitted arc.


\paragraph{Circle events}
If an event is a circle event any disappearing arcs is removed. A vertex is created in the doubly connected edge list, the two edges traced by the beachline is connected to the vertex, and a new edge leaving the vertex is created. New circle events are calculated amongst the remaining neighboring arcs and added to the event queue.

\paragraph{Remaining infinite edges}
To add remaining infinite edges to voronoi diagram, it should merely be noted that these are all still represented by the remaining arcs in the status. So a traversal of the red black tree is made and all neighboring arcs has an edge in between them and this edge is connected to some vertex defined as the bounding box. 

\paragraph{Time complexity}
When handling site or circle events, a constant number of insertions, deletions, queries, and extractions are made, which each takes logarithmic time on the input of size $n$. 
So for each event we use $\Oh (\log n)$ time. 
We know that there is $n$ site events and no more can be added. Additionally since each site represents a face in the resulting voronoi diagram. We can deduce from Eulers theorem that there is at most $2\cdot n - 5$ vertices in the voronoi diagram. Since all handled circle events creates a vertex in the voronoi diagram, there must be a linear number of circle events handled.

Then we know that the algorithm has time complexity $\Oh (n\cdot \log n)$

\subsection*{Geometric calculations}

\paragraph{Parabola-parabola intersection}

An essential geometric test is that of finding the intersection of parabolas forming the beach line.
Luckily this is as easy as finding the roots of a single parabola.
Let the two parabolas be given by $a x^2 + b x + c$ and $d x^2 + e x + f$. We know that these are
equal at the point of intersection thus
\[
    a x^2 + b x + c = d x^2 + e x + f \Leftrightarrow
    (a-d) x^2 + (b-e) x + (c-f) = 0
\]

Now the $x$-coordinate can be found simply by solving the above quadratic using the well-known formula.
\[
    x = \frac{-(b-e) \pm \sqrt{(b-e)^2 - 4(a-d)(c-f)}}{2(a-d)}
\]


Most often this will give two solutions. If $a x^2 + b x + c$ is the left parabola and $d x^2 + e x + f$
is the right, then the solution using plus in the formula above is the solution we are looking for.


\paragraph{Circumscribed circle of three points}

Circle events are created by finding circumscribed circles of the neighboring sites.
To compute this circle we will find the center point and the radius. 

\medskip

It is known that a triangle has a circumscribed circle whose center can be found by the
intersection of the bisectors of the sides. Therefore we will construct a triangle from the three points.
It turns out that only two of the bisectors are actually needed thus only two sides are created.
Next, we find the bisector of these sides and find where they intersect. 
This gives the center point of the circle. Finally, the radius can be found
by finding the distance from the center point to any of the input points.
The scenario is depicted in Figure~\ref{circumscribed-circle}.

\medskip

Be aware of the following case which is not general position: Three colinear points.
This would create an infinitely large circle -- effectively just a line -- with a center point
infinitely far away. This case is detected, but not handled.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{images/circumscribed-circle.png}
    \caption{Finding the circumscribed circle of three points}
    \label{circumscribed-circle}
\end{figure}


\section*{Implementation}

\paragraph{Red-black tree} 
The status of the algorithm will use a red-black tree to ensure $\Oh(\log n)$ operations.
It is implemented based on Cormen, but with the following modifications:

\begin{itemize}
    \item Neighbors: Every node in the tree maintains a pointer to its left and right neighbor (if any).
    \item Sites and arcs: Every node represents an arc of the beach line which is defined by a
        site point saved in node. Additionally, to distinguish arcs from the same site from each other,
        another point is stored in the node. The x-coordinate of this point defines which part of the parabola it is
        and the y-coordinate is the location of the sweepline at the time of insertion.
    %\item Comparison: (Describe pos_epsilon)
    \item Event queue: A node might also point to an element in the event queue, so the event can be skipped in the case of a
        circle event being a false alarm.
    \item DCEL: A left and right half-edge representing the lines being drawn by the
        left and right side of the arc as the sweepline moves down. These are used to connect
        new parts of the DCEL to the existing parts.
\end{itemize}


The comparison in the red-black tree works as follows:

For a given $y$-coordinate of the sweepline and for each arc,
it is possible to define the $x$-interval for the arc
by finding intersections with its neighbors.

When a site is found and the corresponding new arc is inserted,
its $x$-coordinate will be used to find the interval.
At every node it is determined if $x$ is to the left of the interval;
in that case go to the left subtree. Similarly for the right side.
If $x$ is in the interval, the arc that is going to be split has been found.

Similarly, when deleting an arc it is found based on its interval.


\paragraph{Heap}
The event queue is maintained by a max heap on the y coordinate of events. At first deletion was implemented, but as this caused errors very late in implementation, it was removed and now events are simply 
flagged to not be handled if they shouldn't be. This regrettably ruins the asymptotic time complexity as we don't have a bound on the total number of fake circle events. But since it is a very minimalistic heap, 
the constants involved should be low and not affect the running time terribly much. The red black tree could have been used instead as a priority queue, or the deletion could have been fixed. But time was not allocated for this.

% TODO: Make argument for total size of heap if no deletions where there, and argue that the height of the heap won't be larger than this "total" heaps height.

% The heap is placed in an array and the way it is laid out creates a perfectly balanced tree.
% Replacing an element with the last element only removes something from the lowest layer most to the right,
% thus leaving no holes. As the removed element was replaced by the last element,
% the number of elements down any path from the removed element has the name number of elements.
% The bubbling up or down only changes the ordering of the elements on one such path.

\paragraph{DCEL}

The doubly-connected edge list (or DCEL) data structure that will be used to store objects in the Voronoi diagram
once computed. The DCEL is a graph consisting of vertices and edges, which are in the form of half-edges. These
half-edges together form an edge pointing to the two vertices that the halfedges originates from.

\paragraph{Eventhandler}

The eventhandler is the overall algorithm as described in the design section. 
It handles initialization of the priority queue and status, and goes through every event as described in the design section.

When a site event happens, a new halfedge is created between the site and the site corresponding to the arc that this site event splits.
Whenever a circle event happens in the program, a vertex is added
to the DCEL, and corresponding edges are attached to the vertex. New half-edges are also created leaving the vertex, following the intersection of two arcs
on the beachline.

After all events have been run it traverses through the status and adds the remaning infinite half-edges to the DCEL by linking them to a bounding box vertex.
A direction of these halfedges is determined from their adjacent sites and their origin vertex, for visualisation purposes.


%\subsection*{Robustness}
% TODO: Decide if this is needed




\section*{Manual}

\subsection*{Getting Started}

To quickly get started,
create a file e.g. \texttt{test.txt} with points representing sites.
The points are separated by newline and the coordinates for each point
are separated by space as follows

\begin{verbatim}
x1 y1
x2 y2
x3 y3
...
\end{verbatim}

Next, run

\begin{verbatim}
$ ./main.py -i test.txt -v
\end{verbatim}
The flag \texttt{-v} will enable step by step visualization
which will be located in the folder \texttt{steps/}.

The text output will be in \texttt{test.txt.result}
and image output will be in \texttt{test.txt.result.png}.

\medskip

In the following the format and usage will be explained more in-depth.


\subsection*{Input \& Output Format}

An input file contains a list of sites. A site can be formatted as follows: 

\begin{verbatim}
x1 y1
\end{verbatim}

or

\begin{verbatim}
Point(x1, y1)
\end{verbatim}

where \texttt{x1} and \texttt{y1} can be any integer or floating point number.

\medskip

Sites in the input file are separated by newline.

\medskip

Example:

\begin{verbatim}
20 20
81 40
50 10
55 93
30 64
\end{verbatim}

The output file will be line segments representing the edges of the doubly-connected edge list.
Each line will be separated by a newline and have the following format

\begin{verbatim}
(x1, y1) (x2, y2) (inf)
\end{verbatim}

Meaning that a line segment goes from \texttt{(x1,y1)} to \texttt{(x2,y2)}.

\texttt{(inf)} after a line segment indicates that it is infinite,
starting from \texttt{(x1,y1)} passing through \texttt{(x2,y2)}
and continuing infinitely in that direction.

\subsection*{Usage}

\begin{verbatim}
usage: main.py [-h] [-t [N]] [-lb [N]] [-ub [N]] [-d]
               [-v] [-r [N]] [-i FILE] [-o FILE]

Compute the Voronoi diagram of a set of points

optional arguments:
  -h, --help            show this help message and exit
  -t [N], --test [N]    Run tests up to input size N
  -lb [N], --lowerbound [N]
                        The lower bound of possible 
                        random points and drawing canvas
  -ub [N], --upperbound [N]
                        The upper bound of possible 
                        random points and drawing canvas
  -d, --debug           Enable debugging
  -c, --convex          Show convex hull of input points
  -v, --visualize       Enable visualization of each step
  -r [N], --random [N]  Generate a random input of size N 
                        and compute the Voronoi diagram of that
  -i FILE               File containing input
  -o FILE               File where the result will be written
\end{verbatim}

For example, running the algorithm on file \texttt{input1.txt} with visualization for each step can be done as follows:

\begin{verbatim}
$ ./main -v -i input1.txt
\end{verbatim}

which will result in an text output named \texttt{input1.txt.result},
an image output name \texttt{input1.txt.result.png}
and visualization which is placed in the \texttt{steps/} folder.

\medskip

Running

\begin{verbatim}
$ ./main.py -r 30 -lb -200 -ub 200
\end{verbatim}

will generate 30 random points from $-200$ to $200$ on both axes.
The default lower and upper bounds are $0$ and $100$ respectively.
The output of the above will be a text output named \texttt{random.result}
and an image output named \texttt{random.result.png}.

\medskip

Running

\begin{verbatim}
$ ./main.py -t
\end{verbatim}

will launch the test suite.

\section*{Tests}

The following tests as well as a few other can be found as input files in the \texttt{input} folder.
The result of the algorithm on each input file can be found in the \texttt{output} folder.


\subsection*{General tests}

First, $20$ random points (\texttt{input/demo-20.txt}) has been generated.
Using the flag \texttt{-v}, one can see step by step visualization, in the form of a picture being generated per step,
which features the sweepline, the current beachline and the current DCEL
(of which only the finite parts can be determined).
Blue points are sites, black lines and points are DCEL and red points
are circle events.
This is depicted in Figure~\ref{demo-20-halfway}.
The final result can be seen in Figure~\ref{demo-20-result}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/demo-20-halfway.png}
    \caption{A visualization halfway through showing partial DCEL and beachline}
    \label{demo-20-halfway}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/demo-20-result.png}
    \caption{Final result of 20 random points}
    \label{demo-20-result}
\end{figure}


Other tests have been carried out with $5$, $10$, $100$, $1000$ points.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/demo-5.png}
    \caption{5 random points}
    \label{demo-5-result}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/demo-10.png}
    \caption{10 random points}
    \label{demo-10-result}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{images/demo-100.png}
    \caption{100 random points}
    \label{demo-100-result}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/demo-1000.png}
    \caption{1000 random points}
    \label{demo-1000-result}
\end{figure}

In all cases, the implementation seems to handle the input correctly.

\medskip

An interesting property of the remaining arcs and infinite half-edges
is that neighboring sites are those of the convex hull of the
input sites. Thus it is possible -- albeit a bit expensive --
to compute the convex hull of a point set using the flag \texttt{-c}.
A demonstration of this is shown in Figure~\ref{demo-convex}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/demo-convex.png}
    \caption{5 random points with convex hull}
    \label{demo-convex}
\end{figure}




\subsection*{Run time test}
A run time with every fifth input size up until ten thousand was done and a plot of this can be seen in figure \ref{bigtest}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{images/testresults10k.png}
    \caption{tests up to n=10000}
    \label{bigtest}
\end{figure}




\section*{Conclusion}

In this paper an algorithm for computing Voronoi diagrams
from a set of points in general position
has been explained and implemented.


\medskip

The algorithm runs in time $\Oh(n \log n)$, practically this is not the case because of the heap. Any other part of the implementation lives up to the algorithms complexity though.
and tests have been carried out to ensure the correctness
of the implementation.


\newpage

\appendix

\section{Source code}


\addcode{eventhandler.py}

\newpage

\addcode{main.py}

\newpage

\addcode{redblacktree.py}

\newpage

\addcode{heap.py}

\newpage

\addcode{dcel.py}

\newpage

\addcode{geometry/point.py}

\newpage

\addcode{geometry/lineseg.py}

\newpage

\addcode{geometry/circle.py}

\newpage

\addcode{geometry/parabola.py}

\newpage

\addcode{test.py}

\newpage

\addcode{inputoutput.py}


\end{document}
