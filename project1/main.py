#!/usr/bin/env python2

from geometry import *
from inout import *
from redblacktree import *
from eventHandler import *
from test import *

import argparse

parser = argparse.ArgumentParser(description='Rotational plane sweep finding visible line segments')

parser.add_argument("-t", "--test", help="Run tests up to input size N", type=int,
                    nargs="?", metavar = "N", const=200)

parser.add_argument("-lb", "--lowerbound", help="The lower bound of possible random points", type=int,
                    nargs="?", metavar = "N", default=0)
parser.add_argument("-ub", "--upperbound", help="The upper bound of possible random points", type=int,
                    nargs="?", metavar = "N", default=100)

parser.add_argument("-d", "--debug", help="Enable debugging", action="store_true")
parser.add_argument("-v", "--visualize", help="Enable visualization of each step", action="store_true")

parser.add_argument("-i", dest="input_filename",
                    help="File containing input", metavar="FILE")

parser.add_argument("-o", dest="output_filename",
                    help="File where the result will be written", metavar="FILE")

args = parser.parse_args()

if args.test:
    if args.output_filename:
        print "Running tests of input size up to %d...to file %s" % (args.test, args.output_filename)
        run_tests(args.test, 5, args.output_filename, args=args)
    else:
        print "Running tests of input size up to %d..." % args.test
        run_tests(args.test, args=args)
elif args.input_filename:
    if args.output_filename:
        output_filename = args.output_filename
    else:
        output_filename = args.input_filename + ".result"

    print "Loading file '%s'" % args.input_filename
    c, segs = input_from_file(args.input_filename)

    print "Performing sweep line algorithm"
    result = sweep(segs, c, args)

    print "Outputting result to file '%s'" % output_filename
    output_to_file(output_filename, result)
else:
    # Display help message
    parser.parse_args(["-h"])
