from geometry import *
from redblacktree import *

import math
import subprocess

def sweep(lines, center, args):
    # --- Initialization ---

    # This function will be used as the compare
    # function in the Red Black tree
    def compare_on_sweep_line(x, y):
        if x == y or x is y:
            return 0
        if x.line == y.line or x.line is y.line:
            return 0
        return x.isCloserThan(y, sweep_line)

    # Event queue
    event_queue = sort_lines_counterclockwise(lines, center)

    # Construct initial sweepline
    sweep_line = find_initial_sweep_line(center, lines)

    # Construct initial status
    status = Rbt(compare_nodes=compare_on_sweep_line)
    check = initial_status(status, lines, sweep_line)

    # Create a structure to store the result
    # and avoid duplicates
    visible_lines = Rbt()

    if args.visualize:
        visualize_step(center, lines, sweep_line, status, visible_lines, 0, args)

    # If check is a list, it contains a line segment passing through the query point.
    # By design, this is the only visible line segment and thus should be returned.
    if isinstance(check, list):
        if args.visualize:
            visible_lines.insert(check[0])
            visualize_step(center, lines, sweep_line, status, visible_lines, 1, args)
        return check

    i = 1

    # --- Sweep algorithm ---
    for event in event_queue:
        # Create shorthands for the different parts of the event point
        point = event[0]
        line  = event[1]


        # Update the sweep line
        sweep_line = find_sweep_line(center, point)

        if line.start == point: # Start point
            #insert and report closest line as visible
            status.insert(line)
            # Add the visible line
            visible_node = status.rbGetMin()
            visible_line = visible_node.line

            if visible_line is not None:
                visible_lines.insert(visible_line)

            if not turn(center, point, line.other_end(point)):
                # The line is parallel to sweep line
                # remove it from status and tag it so it is "skipped" in event queue
                status.delete(line)
                line.is_removed = True
        elif line.start != point and not line.is_removed: # End point
            status.delete(line) 

            # Add the visible line
            visible_node = status.rbGetMin()
            visible_line = visible_node.line
            if visible_line is not None:
                visible_lines.insert(visible_line)
        
        
        elif line.start != point and line.is_removed:
            print "Just finished dealing with a line where the start and end point are on the sweep line at the same time."
        else: 
            print "something went terribly wrong\n"

        # Visualize step
        if args.visualize:
            visualize_step(center, lines, sweep_line, status, visible_lines, i, args)

        i += 1

    # Store the final result
    result = []
    visible_lines.inOrderTreeWalk(lambda node: result.append(node.line))

    return result





# Determine the line segment that
# could be thought of as the initial sweep line.
# It must start at the center point and extend
# horizontally further out to the right than
# any point on any other line segment.
def find_initial_sweep_line(c, segs):
    # Initially, the x-co we have seen is the x-co of the center point.
    maxX = c.x
    # Go over line segment and update the maxX if any of the endpoints
    # have a greater x-value that our current.
    for seg in segs:
        m = max(seg.p.x, seg.q.x)
        if m > maxX:
            maxX = m

    # Create and return the line segment representing the sweep line
    # from center and maxX out to the right. 10 is added just
    # to be on the safe side.
    initial_sweep_line = find_sweep_line(c, Point(maxX, c.y))
    return initial_sweep_line

# find any line intersecting the initial sweep line
# and place this line in the initial status
def initial_status(status, segs, initial_sweep_line):
    initial_sweep_line.infinite = False
    center_point = initial_sweep_line.p
    for seg in segs:
        if seg.intersects(initial_sweep_line):
            if seg.on_line(center_point):
                # Check if line touches center
                print "THE CENTER POINT IS ON A LINE\n specifically line %s" % seg
                # Possibly stop program here
                return [seg]
            # swap start and end point definitions of line, but only if it's not start or end point of line touching sweepline
            if not initial_sweep_line.on_line(seg.p) and not initial_sweep_line.on_line(seg.q): 
                if seg.start == seg.p:
                    seg.start = seg.q
                elif seg.start == seg.q:
                    seg.start = seg.p
                else:
                    print "something went wrong swapping start and end"
            status.insert(seg)
    initial_sweep_line.infinite = True
    return status

# Create a sweep line as a line segment which
# is twice as long as from the center to the actual point
def find_sweep_line(center, p):
    return LineSeg(center, center + (p-center)*2, infinite=True)

def visualize_step(center, lines, sweep_line, status, visible_lines, i, args):
    num_lead_zeros = int(math.log(2*len(lines))/math.log(10)) + 1

    status_dot = status.toDot()
    dot_file = ("steps/status%0"+str(num_lead_zeros)+"d.dot") % i
    with open(dot_file, 'w') as f:
        f.write(status_dot)
    subprocess.call(["dot", "-Tpdf", dot_file, "-o", ("steps/status%0"+str(num_lead_zeros)+"d.pdf") % i])

    plot_linesegs(lines, color="r")

    status_list = []
    status.inOrderTreeWalk(lambda node: status_list.append(node.line))
    plot_linesegs(status_list, color="y")

    result = []
    visible_lines.inOrderTreeWalk(lambda node: result.append(node.line))
    plot_linesegs(result, color="g")

    plot_points([center])
    plot_linesegs([sweep_line], color="b")

    plt.xlim([args.lowerbound,args.upperbound])
    plt.ylim([args.lowerbound,args.upperbound])

    plt.savefig(("steps/plot%0"+str(num_lead_zeros)+"d.png") % i)
    plt.clf()
