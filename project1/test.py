from geometry import *
from datetime import datetime
from eventHandler import *


def test(n, args = None):
    c = 0
    for i in range (1, 5):
        # Input
        p = random_point()

        segs = random_disjoint_linesegs(n, lb=args.lowerbound, ub=args.upperbound)

        # Timetaking
        a = datetime.now()
        
        try:
            result = sweep(segs, p, args)
        except TypeError:
            print "=== None error! Printing relevant information ==="
            print p, segs

        b = datetime.now()
        c = c + (b - a).total_seconds()
    
    result = c/5.0
    return n, result

def run_tests(n,i = 5, filename = 'testresults', args = None):
    results = []
    while i <= n:
        result = test(i, args)
        results.append(result)
        i = i + 5
    
    f = open(filename, 'w')
    f.write('\n'.join('%s %s' % x for x in results))

