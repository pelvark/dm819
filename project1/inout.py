#delicious burgers found here!
import re
from geometry import *

def input_from_file(filename):
    re_num = r"([-+]?[0-9]*\.?[0-9]*)"
    re_point = r"(Point)?\(%s,\s*%s\)" % (re_num, re_num)
    re_line1 = r"%s -+ %s" % (re_point, re_point)
    re_line2 = r"LineSeg(%s,\s*%s)" % (re_point, re_point)
    # Read the file line by line and extract center point
    # and line segments
    with open(filename) as f:
        file_lines = f.readlines()
        center = None
        lines = []
        for file_line in file_lines:
            # Search for line segment
            m = re.search(re_line1, file_line)
            if m:
                px,py,qx,qy = m.group(2), m.group(3), m.group(5), m.group(6)
                p = Point(float(px), float(py))
                q = Point(float(qx), float(qy))
                line = LineSeg(p, q)
                lines.append(line)
                continue
            # Search for center point
            m2 = re.search("center: %s" % re_point, file_line)
            if m2:
                px,py = m2.group(2), m2.group(3)
                center = Point(float(px), float(py))

        if center is None:
            print "No center point given."
            return None

        if lines == []:
            print "No line segments given."
            return None

        return (center, lines)

def output_to_file(filename, output):
    with open(filename, 'w+') as f:
        for line in output:
            f.write("%s\n" % str(line))
