\documentclass[12pt, a4paper]{article}

% Font & text
\usepackage{times}
\usepackage[utf8]{inputenc} 
\setlength{\parindent}{0in}
\usepackage{hyperref}
\hypersetup{%
    pdfborder = {0 0 0}
}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}

% Math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}

\newcommand{\im}{\mathrm{Im}}
\newcommand{\p}[1]{\left( #1 \right)}
\newcommand{\lr}[3]{\left#1 #3 \right#2}
\newcommand{\st}{\mathrm{s.t.}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\B}{\mathbb{B}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\atan}{\mathrm{atan}}
\newcommand{\atantwo}{\mathrm{atan2}}
\newcommand{\Oh}{\mathcal{O}}
\newcommand{\lineseg}[1]{\overline{#1}}

% Source code
\usepackage{listings}
\usepackage{color}

\newcommand{\addcode}[1]{
\subsection*{#1}
\lstinputlisting[language=python, xleftmargin=-5em,xrightmargin=-5em]{../#1}
}

\lstset{
    language=Python,
    showstringspaces=false,
    breaklines=true,
    breakatwhitespace=false,
    formfeed=\newpage,
    tabsize=4,
    numbers=left,
    numbersep=5pt,
    numberstyle=\tiny\color{gray},
    commentstyle=\itshape\color{gray},
    keywordstyle=\bfseries\color{blue!40!black},
    identifierstyle=\color{black},
    stringstyle=\color{green!50!black},
    basicstyle=\ttfamily,
    morekeywords={lambda}
}


% Algorithms
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}

\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother


% Proofs
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}

\renewcommand\qedsymbol{$\blacksquare$}


% Page, Header & Footer
%\usepackage{geometry}
\pagestyle{empty}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\cfoot{}
\rfoot{\thepage\ of \pageref{LastPage}}
\rhead{}

% Images
\usepackage{tikz}

\usepackage{titling}
\date{5th October 2015}
\author{Peter Severin Rasmussen \and Jon Kingo Christensen}
\def\identification{Peter Severin Rasmussen, 250394, peras13@student.sdu.dk \\ Jon Kingo Christensen, 180491, jonch10@student.sdu.dk}
\def\courseCode{DM819}
\def\courseTitle{Computational Geometry}
\def\lectorLabel{Teacher:}
\def\lector{Kim Skak Larsen}
\def\universityName{Syddansk Universitet}
\def\papertitle{Assignment 1}
\def\papersubtitle{\courseTitle}
\title{\papertitle{} - \papersubtitle{}}




% ============ %
\begin{document}
%\maketitle

\begin{tabular}{@{}l}
\universityName{} \\
\courseTitle{} (\courseCode{}) \\
\lectorLabel{} \lector{}
\end{tabular}
\hfill
\begin{tabular}{r@{}}
\identification{} \\
\thedate{}
\end{tabular}

\bigskip

% Frontpage Title
\begin{center}
    \Huge{\papertitle{}} \\
    \Large{\papersubtitle{}}
\end{center}
\normalsize{}


%\bigskip

%\newpage

%\restoregeometry{}



\section*{Specification}

In the following paper a plane sweep algorithm will be implemented
to solve the problem of determining which walls
that are visible from a given standing point.

\medskip

More formally, the algorithm is given 
a list of $n$ disjoint line segments, $S$,
and a query point, $p$, and it must determine the subset of $S$
that is visible. A line segment $l$ is said to be visible from $p$
if there exists a point $q$ on $l$ such that $\lineseg{pq}$ is
not intersected by any other line segment.

\medskip

The program will take the mentioned input from a text file. % Describe format?

The text output is a list (without duplicates) of all the line segments from $S$ that are visible from $p$.

\subsection*{Special cases} 

Besides the formal specification of the problem, some choices have to be made with regard to special cases and how to react to these. The choices made for this is specified as follows\\
\textbf{Parallel lines:} If a line is parallel to the sweepline when handling it, the program should be able to see it (if it's not blocked by anything). And the parallel line should not block the view of anything behind it.\\ 
\textbf{Line going through query:} If a line goes through the query point, the program should only be able to see this line, as it "blocks" everything else.\\
\textbf{Two lines sharing an end point:} The input should not include lines sharing an end point, as this is not a disjoint line.\\
\textbf{Lines starting and ending at same angle} When lines start and end at the same angle, there could be a third line behind both of these lines. This third line should not be seen as visible.




\section*{Design}

The problem is solved using a plane sweep algorithm where a half line traverses counterclockwise around the query point. 

The status is all lines currently intersecting the sweep line. These are stored in a red black tree with respect to distance from the query point to the intersection points of the sweep line and the corresponding line segment.

The event queue is all the end points of the line segments.
There are $2 n$ of these. As the line segments are disjoint, 
it is not necessary to add or remove any point 
from the event queue once initialized. 
Therefore it is enough to simply keep event points in a list, 
sort it as described later and then take each event point in sequence.

After the creation of the event queue, 
we determine which lines should be in the initial status 
according to the initial sweep line 
which we choose to be a horizontal line extending out to the right
from the query point.

After the initial construction, we traverse through our event queue,
updating the sweep line to pass through that point 
and making sure that for any point in time, any line crossing the sweep line will be in the status (correctly sorted). 

Then it follows that any line that is at some point the "minimum node" in the red black tree is visible from the query point.

\medskip

This algorithm, before accounting for special cases, can be seen as pseudocode in algorithm~\ref{algorithm_1}.

\begin{algorithm}
    \caption{Rotational plane sweep}\label{algorithm_1}
    \begin{algorithmic}[1]
        \Procedure{Sweep}{$S$, $p$}
            \State $\mathcal{Q} \gets \text{Create event queue}$
            \State $\mathcal{T} \gets \text{Create initial status}$
            \ForAll{$\textit{e} \in \mathcal{Q}$}
                \If {$\textit{e}$ is start point}
                    \State Insert line of corresponding event point into $\mathcal{T}$
                    \State Report minimum of $\mathcal{T}$ as visible line
                \ElsIf {$\textit{e}$ is end point}
                    \State Delete line from $\mathcal{T}$
                    \State Report minimum of $\mathcal{T}$ as visible line
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}


\medskip

Creating the event queue is done by sorting all endpoints counterclockwise around the query point.
The angle is measured between a vector parallel with the x-axis and the vector from the query point to endpoint.
In case of a tie, a start point will be favored over an end point.
If that is also a tie, the point closest to the query point is chosen.

This takes $\Oh(n \log n)$ operations, since it is just sorting $2 n$ points using angles as their values.

Initializing the status takes at most $\Oh(n \log n)$ operations, because for each line we can check in a constant number of operations, whether it intersects with the initial sweepline. And if it does we need to insert it into the red black tree.

The overall complexity of algorithm~\ref{algorithm_1} is then the construction and the for loop of size n, wherein all operations are either insertions, deletions or getting the minimum in a red black tree which has complexity $\Oh(\log n)$. Thus the algorithm has a complexity of $\Oh(n \log n)$.

\subsection*{Geometric tests}


\paragraph{Line-line intersection}

There are multiple cases depending on whether 
neither, one or both lines are infinite.

Let $(x_1,y_1)(x_2,y_2)$ and $(x_3,y_3)(x_4,y_4)$ be two
lines passing through the corresponding two points.
Using Cramer's rule one will obtain
that the intersection point $(p_{x}, p_{y})$ is given by

\begin{align*}
    (p_{x}, p_{y}) =& \left(
\frac{(x_1 y_2 - y_1 x_2) (x_3 - x_4) - (x_1 - x_2) (x_3 y_4 - y_3 x_4)}
    {(x_1 - x_2) (y_3 - y_4) - (y_1 - y_2) (x_3 - x_4)},\right. \\
    &\left.\frac{(x_1 y_2 - y_1 x_2) (y_3 - y_4) - (y_1 - y_2) (x_3 y_4 - y_3 x_4)}
    {(x_1 - x_2) (y_3 - y_4) - (y_1 - y_2) (x_3 - x_4)} \right)
\end{align*}

If the denominator is $0$, the lines are parallel.
For the purpose of finding an intersection point,
one can check whether one of the four points
are on the line segment, suggesting that the two lines coincide.

\medskip

If one line is infinite and another is not, 
we can simply consider both infinite, find the intersection
as mentioned above and check if the intersection point is
on the finite line segment.

\medskip

And if both lines are finite, 
we can simply consider both lines infinite, find the intersection
and check if the intersection point is on both finite line segments.


\paragraph{Point on line segment}

To determine if a point is on a line,
we use the degenerated case of the triangle inequality,
namely when the inequality becomes equality.

Given a line segment $\lineseg{pq}$ and a point $r$,
consider the triangle $pqr$. All the length of the sides
can be calculated by finding the distance between the different
points of the triangle. 
As long as this triangle is not degenerate, i.e. not a line,
we can say for certain that $r$ is not on $\lineseg{pq}$.
Using the triangle inequality we can therefore say that
$r$ is on the line segment $\lineseg{pq}$ if and only if

\[
    |\lineseg{pr}| + |\lineseg{qr}| = |\lineseg{pq}|
\]


\paragraph{Counter clockwise angle between vectors}

The normal formula for the angle between two vectors 
which involves $\cos$ finds the smallest of the two spanned angles. 
In our case we need to sort points by the same angle no matter,
how they are placed, i.e. we need to find the counter clockwise
angle. To this end, it is possible to use a formula involving $\sin$.
But by far the easiest solution is to use a function called $\atantwo$.
It calculates the angle of the slope as the normal $\atan$ except
that it also deals with special cases such that if the line
is vertical, i.e. has infinite slope. 
The image of $\atantwo$ is $(-\pi, \pi]$. 
In our case we need a range of $[0, 2\pi)$ thus for all negative
results of $\atantwo$ we add $2\pi$.


\section*{Implementation}
The solution was done in the python programming language following the pseudocode in algorithm~\ref{algorithm_1} and taking care of special cases without jeopardizing the complexity.

\paragraph{Red black tree}
A red black tree implementation was made using the pseudocode in Introduction to Algorithms, 3rd Edition, by Thomas Cormen. The only essential change needed was making a customized compare function \textit{isCloserThan(self, other, sweep\_line)}, that answers which, of two lines, is closer to the query point.

\paragraph{Geometry}
All the geometric computations needed, was written within the classes \textit{Point} and \textit{LineSeg}. These can be found in the \textit{geometry.py} file. The math behind these computations can be read in the geometric tests section.

\paragraph{Event queue}
The event queue is constructed by, for each end point of a line, creating a tuple with the point and its corresponding line in it. Within the line object the start point of said line is saved for later reference. Then all these tuples can be sorted using pythons own sorting function along with a compare function to use. This compare function first considers the angle of the two points. If these are the same then all start points should come before end points. And if these are the same then the point with the smallest distance to the query point should come first. All this is implemented in the \textit{geometry.py} file, in the \textit{cmp\_counterclockwise\_around} and \textit{sort\_lines\_counterclockwise} functions.  

\paragraph{Initial status}
The initial status is made by setting up the initial sweep line and then checking all lines to see whether they intersect with the sweep line. Those that do is corrected in regards to which point is the start point. Because the initial sorting just assigned this from the angle of the points. They are then put into the status and the initial status is made.  

\paragraph{Eventhandler}
The actual algorithm shown in algorithm~\ref{algorithm_1} is implemented in the \textit{eventHandler.py} file much like the pseudocode. After handling an event point, we perform a \textit{getMin} operation on the status to get the line closest to the query point, this line is reported visible. The reported visible lines are stored in a red black tree for easy storage and avoidance of duplicates, and reported in the end of the program. The only real difference from the pseudocode is the part taking care of the special case where a line is parallel to the sweepline when handling it. This is handled by, for each start point event, checking whether the corresponding line is parallel to the sweepline. If this is the case, then the line is checked for visibility from the query point, and reported if it is visible. After this the line is instantly removed from the status and a flag in the line object is set. Then when handling an end point, if this flag is set for its line it means that the line has already been removed from the status and the end point does not need to be handled. This makes a parallel line unable to block another line. 


\subsection*{Robustness}

When dealing with geometric problems, one has to consider robustness of the algorithm.

For instance, when the sweep line reaches a given event point and
the line segment representing the sweep line is constructed between the query point and the event point, 
the status has to be updated.
As the sweep line has just be constructed specifically to go through the event point of some
line segment it should intersect with that point. 
However if one were to find intersection of line segment and line segment,
in rare cases this point will not be found due to rounding errors,
where the intersection point will be considered just outside the line segment.

To cope with problems like this, multiple techniques have been used. 
For instance in the problem above, the line segments are considered infinite
to ensure that some intersection point is found. 

Another technique is used when testing for equality. For instance when testing if a point
is on a line using the triangle inequality, rounding errors could cause
the point to appear just a bit off. To handle this, the two values are simply
considered equal if they are only an $\varepsilon$ away from one another.

In code this

\begin{lstlisting}
def on_line(self, c):
    return self.p.distance_to(c) + self.q.distance_to(c) == self.p.distance_to(self.q)
\end{lstlisting}

becomes this

\begin{lstlisting}
def on_line(self, c, epsilon=0.0001):
    return abs((self.p.distance_to(c) + self.q.distance_to(c)) - self.p.distance_to(self.q)) < epsilon
\end{lstlisting}



\section*{Tests}


\subsection*{Manual}

\begin{verbatim}
usage: main.py [-h] [-t [N]] [-lb [N]] [-ub [N]] [-d] [-v] [-i FILE] [-o FILE]

Rotational plane sweep finding visible line segments

optional arguments:
  -h, --help            show this help message and exit
  -t [N], --test [N]    Run tests up to input size N
  -lb [N], --lowerbound [N]
                        The lower bound of possible random points
  -ub [N], --upperbound [N]
                        The upper bound of possible random points
  -d, --debug           Enable debugging
  -v, --visualize       Enable visualization of each step
  -i FILE               File containing input
  -o FILE               File where the result will be written
\end{verbatim}

For example, running the algorithm on file \texttt{input1.txt} with visualization for each step can be done as follows:

\begin{verbatim}
$ ./main -v -i input1.txt
\end{verbatim}

which will result in an output file named \texttt{input1.txt.result}
and visualization which is placed in the \texttt{steps/} folder.



\subsection*{Input \& Output Format}

Input files can be formatted as follows: 

Define the query point with \texttt{center: <Point>} on its own line.

Define lines with \texttt{<Point> -- <Point>} or \texttt{LineSeg(<Point>, <Point>)}, each on its own line.
\texttt{<Point>} can be \texttt{(x, y)} or \texttt{Point(x, y)}.

\medskip

The output will be a list of line segments, each on its own line, as

\texttt{LineSeg(Point(x1,y1), Point(x2,y2))}.

\medskip

The following tests as well as a few other can be found as input files in the \texttt{input} folder.
The result of the algorithm on each input file can be found in the \texttt{output} folder.


\subsection*{General tests}
Testing the program on input in general position reports the correct lines as visible, in figure~\ref{demo1-result} the graphical output of a test is shown, where visible lines are green.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{images/demo1-result.png}
    \caption{Result of sweep line algorithm. 
    Green line segments are visible.}
    \label{demo1-result}
\end{figure}

The program was also tested for time, with a test for every fifth input size up to 5000 random lines with a random query point. The resulting times for different input sizes can be seen in figure~\ref{big-test}. The results were also plotted with the x axis being an $n \log n$ function, which should result in a linear plotting of the results, granted that the running time follows an $\Oh(n \log n)$ complexity. This plotting is also shown in figure~\ref{big-test}. It does appear linear, although a larger test would be needed for a more precise plotting. Since generating sets of random disjoint linesegments took a lot of time, we only tested up to inputs of 5000 lines. The plotting has some variations that appear large, but we ignored that since they were at most variations of 0,02 seconds, so we assumed that the constant of the complexity were simply larger due to special cases.  

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\linewidth]{images/big-test-n.png}
        \label{big-test-n}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\linewidth]{images/big-test-n-log-n.png}
        \label{big-test-n-log-n}
    \end{subfigure}
    \caption{Results from time testing on different input.}
    \label{big-test}
\end{figure}



\subsection*{Special cases}

\subsubsection*{Line parallel with sweep line}
If a line is parallel to the sweep line when it is beinh handled, it is reported as visible if nothing is blocking its view to the query point.
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{images/special1-result.png}
        \label{special1-result}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{images/special1-parallel.png}
        \label{special1-parallel}
    \end{subfigure}
    \caption{The sweep line is parallel with a line segment
        which will be marked as visible.}
\end{figure}


\subsubsection*{Line crossing query point}
If the query point lies on a line, that line blocks everything else and is the only line reported. This is handled during the construction of the initial status. An example of this is shown in figure~\ref{special2-result}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{images/special2-result.png}
    \caption{A line segment is passing through the query point making it the only visible.}
    \label{special2-result}
\end{figure}

\subsubsection*{Line blocking other line only by end point}
If a line is blocked only by another lines end point, that lines should not be reported as visible. This can be seen in figure~\ref{line-block-endpoint} where the situation arises (above and to the right of the query point), and is handled correctly.

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{images/special4-result.png}
        \label{special4-result}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{images/special5-result.png}
        \label{special5-result}
    \end{subfigure}
    \caption{The end point of a line blocks the entire parallel line from being seen.
        This works for both start and end points}
    \label{line-block-endpoint}
\end{figure}


\subsubsection*{Lines with end points in the status at the same time}
If two lines have the same angle in their points, the closest line is handled first and correctly blocks the line behind it. An example of this can be seen in figure~\ref{special7-result}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\linewidth]{images/special7-result.png}
    \caption{Two lines starting at same angle, where the closer one blocks the other line.
    It works the same if two lines stop at the same angle.}
    \label{special7-result}
\end{figure}

\section*{Conclusion}
The program runs as expected for all tested special cases and general position cases. The algorithm has a complexity of $\Oh(n \log n)$ and testing on random input for many input sizes reveals the running time of the actual program to appear to be $\Oh(n \log n)$ as well.

%\newpage
%
%\appendix
%
%\section{Source code}
%
%
%\addcode{eventHandler.py}
%
%\newpage
%
%\addcode{main.py}
%
%\newpage
%
%\addcode{geometry.py}
%
%\newpage
%
%\addcode{redblacktree.py}
%
%\newpage
%
%\addcode{test.py}
%
%\subsection*{plot.r}
%\lstinputlisting[language=r, xleftmargin=-5em]{../plot.r}
%
%\newpage
%
%\addcode{inout.py}


\end{document}
