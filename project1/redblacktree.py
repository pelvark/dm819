from geometry import *


class Node(object):
    def __init__(self, l, nil):
        self.nil = nil
        self.line = l
        self.left = nil
        self.right = nil
        self.parent = nil
        self.redColor = False

    def __cmp__(self, other):
        return cmp(self.line, other.line)

    # Determine if a line is closer to the center point
    # than another. First we find the intersections
    # of the two points with the sweep line.
    # Then find the distance to the center point.
    def isCloserThan(self, other, sweep_line):
        center_point = sweep_line.p

        # Consider the lines in the status as infinite to ensure that
        # an intersection poin always exists
        self.line.infinite = True
        i1 = self.line.intersects(sweep_line)
        self.line.infinite = False
        if i1 is None:
            print "No intersection between %s and sweep line %s" % (self.line, sweep_line)
        d1 = center_point.distance_to(i1)

        other.line.infinite = True
        i2 = other.line.intersects(sweep_line)
        other.line.infinite = False
        if i2 is None:
            print "No intersection between %s and sweep line %s" % (other.line, sweep_line)
        d2 = center_point.distance_to(i2)

        return cmp(d1, d2)

    def inOrderTreeWalk(self, func):
        if self.left is not self.nil:
            self.left.inOrderTreeWalk(func)

        func(self)

        if self.right is not self.nil:
            self.right.inOrderTreeWalk(func)

    def __str__(self):
        if self is self.nil:
            return "<Nil>"
        if self.left is self.nil and self.right is self.nil:
            label = "Leaf"
        else:
            label = "Node"
        color = "red" if self.redColor else "black"
        return "<%s %s, %s>" % (label, self.line, color)

class Rbt(object):
    def __init__(self, create_node=Node, compare_nodes=cmp):
        self.nil = create_node(None, None)
        self.nil.nil = self.nil
        self.nil.left = self.nil
        self.nil.right = self.nil
        self.nil.parent = self.nil
        self.root = self.nil
        self.create_node = create_node
        self.compare_nodes = compare_nodes

    def insert(self, line):
        self.rbInsert(self.create_node(line, self.nil))

    def rbInsert(self, z):
        y = self.nil
        x = self.root
        while x is not self.nil:
            y = x
            comparison = self.compare_nodes(z, x)
            if comparison < 0: # z.key < x.key
                x = x.left
            elif comparison == 0:
                # Ignore duplicates
                return
            else:
                x = x.right
        z.parent = y
        if y is self.nil:
            self.root = z
        elif self.compare_nodes(z, y) < 0: # z.key < y.key
            y.left = z
        elif self.compare_nodes(z, y) == 0:
            # Ignore duplicates
            return
        else:
            y.right = z
        z.left = self.nil
        z.right = self.nil
        z.redColor = True
        self.rbInsertFixup(z)

    def rbInsertFixup(self, z):
        while z.parent.redColor == True:
            if z.parent is z.parent.parent.left:
                y = z.parent.parent.right
                if y.redColor == True:
                    # case 1
                    z.parent.redColor = False
                    y.redColor = False
                    z.parent.parent.redColor = True
                    z = z.parent.parent 
                else: 
                    if z is z.parent.right:
                        # case 2
                        z = z.parent    
                        self.leftRotate(z)
                    # case 3
                    z.parent.redColor = False
                    z.parent.parent.redColor = True
                    self.rightRotate(z.parent.parent)
            # Whole if thing mirrored
            elif z.parent is z.parent.parent.right:
                y = z.parent.parent.left
                if y.redColor == True:
                    #case 1
                    z.parent.redColor = False
                    y.redColor = False
                    z.parent.parent.redColor = True
                    z = z.parent.parent
                else:
                    if z is z.parent.left:
                        #case 2
                        z = z.parent
                        self.rightRotate(z)
                    z.parent.redColor = False
                    z.parent.parent.redColor = True
                    self.leftRotate(z.parent.parent)
        self.root.redColor = False


    def leftRotate(self, x):
        y = x.right
        x.right = y.left
        if y.left is not self.nil:
            y.left.parent = x
        y.parent = x.parent
        if x.parent is self.nil:
            self.root = y
        elif x is x.parent.left:
            x.parent.left = y
        else: 
            x.parent.right = y
        y.left = x
        x.parent = y


    def rightRotate(self, x):
        y = x.left
        x.left = y.right
        if y.right is not self.nil:
            y.right.parent = x
        y.parent = x.parent
        if x.parent is self.nil:
            self.root = y
        elif x is x.parent.right:
            x.parent.right = y
        else:
            x.parent.left = y
        y.right = x
        x.parent = y                            


    def rbTransplant(self, u, v):
        if u.parent is self.nil:
            self.root = v
        elif u is u.parent.left:
            u.parent.left = v
        else:
            u.parent.right = v
        v.parent = u.parent

    def find(self, line):
        # Create a node around the line
        # such that we can use it for comparison
        tmp = self.create_node(line, self.nil)

        # Start at the root
        x = self.root
        while x is not self.nil:
            comparison = self.compare_nodes(tmp, x)
            if comparison == 0:
                return x
            elif comparison < 0:
                x = x.left
            else:
                x = x.right
        return None


    def delete(self, line):
        found = self.find(line)
        if found is not None:
            self.rbDelete(found)
        else:
            # "Deleting line %s, but it is not present in the tree" % line
            return


    def rbDelete(self, z):
        y = z
        yColorTmp = y.redColor
        if z.left is self.nil:
            x = z.right
            self.rbTransplant(z, z.right)
        elif z.right is self.nil:
            x = z.left
            self.rbTransplant(z, z.left)
        else:
            y = self.rbGetMin(z.right)
            yColorTmp = y.redColor
            x = y.right
            if y.parent is z:
                x.parent = y
            else:
                self.rbTransplant(y, y.right)
                y.right = z.right
                y.right.parent = y
            self.rbTransplant(z,y)
            y.left = z.left
            y.left.parent = y
            y.redColor = z.redColor
        if yColorTmp == False:
            self.rbDeleteFixup(x)


    def rbDeleteFixup(self, x):
        while x is not self.root and x.redColor == False:
            if x is x.parent.left:
                w = x.parent.right
                if w.redColor == True:
                    #case 1
                    w.redColor = False
                    x.parent.redColor = True
                    self.leftRotate(x.parent)
                    w = x.parent.right
                if w.left.redColor == False and w.right.redColor == False:
                    #case 2
                    w.redColor = True
                    x = x.parent
                else:
                    if w.right.redColor == False:
                        #case 3
                        w.left.redColor = False
                        w.redColor = True
                        self.rightRotate(w)
                        w = x.parent.right
                    #case 4 
                    w.redColor = x.parent.redColor
                    x.parent.redColor = False
                    w.right.redColor = False
                    self.leftRotate(x.parent)
                    x = self.root
            else:
                w = x.parent.left
                if w.redColor == True:
                    #case 1
                    w.redColor = False
                    x.parent.redColor = True
                    self.rightRotate(x.parent)
                    w = x.parent.left
                if w.right.redColor == False and w.left.redColor == False:
                    #case 2
                    w.redColor = True
                    x = x.parent
                else:
                    if w.left.redColor == False:
                        #case 3
                        w.right.redColor = False
                        w.redColor = True
                        self.leftRotate(w)
                        w = x.parent.left
                    #case 4
                    w.redColor = x.parent.redColor
                    x.parent.redColor = False
                    w.left.redColor = False
                    self.rightRotate(x.parent)
                    x = self.root
        x.redColor = False      


    def rbGetMin(self, starting_at = None):
        if starting_at is None:
            x = self.root
        else:
            x = starting_at

        while x.left is not self.nil:
            x = x.left
        return x

    def inOrderTreeWalk(self, func):
        if self.root is not self.nil:
            self.root.inOrderTreeWalk(func)


    def toDot(self):
        def node_to_dot(node):
            color_str = "red" if node.redColor else "black"
            label = str(node.line) if node is not self.nil else "<Nil>"
            return "  \"%s\" [label=\"%s\",color=%s]; \n" % (id(node), label, color_str)

        def link_to_dot(parent, child, dir_str):
            return "  \"%s\" -> \"%s\" [label=\"%s\"]; \n" % (id(parent), id(child), dir_str)

        node_queue = [self.root]
        node_str = ""
        link_str = ""

        while len(node_queue) > 0:
            parent = node_queue.pop()
            node_str += node_to_dot(parent)
            if parent.right is not self.nil:
                link_str += link_to_dot(parent, parent.right, "right")
                node_queue.append(parent.right)
            if parent.left is not self.nil:
                link_str += link_to_dot(parent, parent.left, "left")
                node_queue.append(parent.left)

        return "digraph {\n%s\n%s\n}" % (node_str, link_str)
