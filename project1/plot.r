require(lattice)
input = read.table("testresults")

x <- input[1]
y <- input[2]
colnames(input) = c("n", "time")

png(filename="testrun.png", height=295, width=300, bg="white")
#par(mar=c(4.2, 3.8, 0.2, 0.2))
#plot(y*log(y) ~x) 
#above needs some fixin probably, not tested.

xyplot(data = input, time ~n*log(n), type='o')

dev.off()
