import math

#########################################
#   Geometric data structures/classes   #
#########################################

# Point in 2D
# Implements operations on points, such as +,-,*,/ etc
# as well as distance, dot product and determinant/cross product
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)

    def __sub__(self, p):
        return self + (-p)

    def __mul__(self, t):
        return Point(self.x * t, self.y * t)

    def __div__(self, t):
        return Point(self.x / float(t), self.y / float(t))

    def __pos__(self):
        return self.clone()

    def __neg__(self):
        return self * (-1)

    def __cmp__(self, p):
        return cmp(self.to_tuple(), p.to_tuple())

    def __str__(self):
        #return "(%s, %s)" % (self.x, self.y)
        return self.__repr__()

    def __repr__(self):
        return "Point(%s, %s)" % (self.x, self.y)

    def to_tuple(self):
        return (self.x, self.y)

    def clone(self):
        return Point(self.x, self.y)

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def distance_to(self, p):
        return (self - p).length()

    def signed_angle_between(self, p):
        # Based on http://stackoverflow.com/a/2150475
        a = math.atan2(self.det(p), self.dot(p))
        return a if a >= 0 else a + 2*math.pi

    def dot(self, p):
        return self.x * p.x + self.y * p.y

    def det(self, p):
        return self.x * p.y - self.y * p.x


# Line segment defined by left endpoint, p, and right endpoint, q.
# Supports finding intersection with other line segment
class LineSeg:
    def __init__(self, p, q, infinite = False):
        self.p = p
        self.q = q
        self.is_removed = False
        self.start = None
        self.infinite = infinite

    def __cmp__(self, l):
        return cmp(self.to_tuple(), l.to_tuple())

    def __str__(self):
        #return "%s - %s" % (self.p, self.q)
        return self.__repr__()

    def __repr__(self):
        return "LineSeg(%s, %s)" % (self.p, self.q)

    def dir(self):
        return (self.q - self.p)

    def to_tuple(self):
        return (self.p, self.q)

    def other_end(self, point):
        if point == self.p:
            return self.q
        elif point == self.q:
            return self.p
        else:
            print "Something is wrong! %s is neither end points of %s" % (point, self)

    def parallel_with(self, other, epsilon=0.00001):
        dir1 = self.dir()
        dir2 = other.dir()
        a1 = dir1.signed_angle_between(dir2)
        a2 = dir1.signed_angle_between(-dir2)
        return a1 < epsilon or a2 < epsilon

    # Check if point is on the line.
    def on_line(self, c, epsilon=0.0001):
        return abs((self.p.distance_to(c) + self.q.distance_to(c)) - self.p.distance_to(self.q)) < epsilon

    # Find intersection between lines
    def intersects(self, l):
        if not isinstance(l, LineSeg):
            print "Intersection betweeen line and %s is not supported" % type(other)
            return

        if self.infinite and l.infinite:
            x1, y1 = self.p.to_tuple()
            x2, y2 = self.q.to_tuple()
            x3, y3 =    l.p.to_tuple()
            x4, y4 =    l.q.to_tuple()
            denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
            if denominator == 0:
                # Parallel lines
                if   self.on_line(l.p): return l.p
                elif self.on_line(l.q): return l.q
                elif l.on_line(self.p): return self.p
                elif l.on_line(self.q): return self.q
                else: return None # Disjoint lines
            x_numerator = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)
            y_numerator = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)
            x = x_numerator / float(denominator)
            y = y_numerator / float(denominator)
            return Point(x, y)
        elif self.infinite and not l.infinite:
            # Find the intersection as if both lines are infinite
            l.infinite = True
            intersection = self.intersects(l)
            l.infinite = False
            if intersection is None:
                return None
            # Check if the intersection is on the line segment
            if l.on_line(intersection):
                return intersection
            else:
                return None
        elif not self.infinite and l.infinite:
            return l.intersects(self)
        else: # not self.infinite and not l.infinite
            (p,r) = (self.p, self.dir())
            (q,s) = (l.p, l.dir())
            d = r.det(s)
            if d == 0:
                k = (q-p).det(r)
                if k == 0: # Colinear
                    # Check for overlapping interval here
                    return None
                else:
                    return None
            else:
                t = (q-p).det(s/d)
                u = (q-p).det(r/d)
                if 0 <= t <= 1 and 0 <= u <= 1:
                    return p + r * t
                else:
                    return None


# Determine whether three consecutive points does a left turn,
# right turn or are colinear

# Define constants for better readability
LEFT_TURN, RIGHT_TURN, COLINEAR = (1, -1, 0)

# Returns 1, -1, 0 if p,q,r forms a left turn, a right turn or are colinear.
def turn(p, q, r):
    return cmp(p.det(q) + r.det(p) + q.det(r), 0)

# Create a compare function
# to sort points counterclockwise around a given center point
# Inspired by http://stackoverflow.com/a/6989383
def cmp_counterclockwise_around(c):
    # The unit vector for the x-axis as reference
    ref = Point(1,0)
    # When we have a center point c
    # We create a compare function taking two points.
    # It returns
    #   -1 if p < q
    #    0 if p == q
    #    1 if p > q
    # where the ordering is counterclockwise around c.
    def cmp_counterclockwise(event_p, event_q):
        p      = event_p[0]
        q      = event_q[0]
        line_p = event_p[1]
        line_q = event_q[1]

        # If the two points are the same,
        # report that the are equal.
        if p == q:
            return 0

        # Move the two vectors to the origin
        # relative to the point c
        # and find the angle between them.
        a_p = ref.signed_angle_between(p - c)
        a_q = ref.signed_angle_between(q - c)

        # Find the one with the smallest angle
        if a_p < a_q:
            return -1
        elif a_p > a_q:
            return 1

        # In case of a tie, a start point should come before an end point
        if line_p.start is p and line_q.start is not q: # p is a start point and q is not
            return -1
        elif line_p.start is not p and line_q.start is q: # p is not a start point and q is
            return 1

        # In case of a tie, use distance

        # Calculate the distance between the query point and the points
        dist_p = c.distance_to(p)
        dist_q = c.distance_to(q)

        # If both are start points, find the one closest to the query point
        if line_p.start is p and line_q.start is q:
            return -1 if dist_p < dist_q else 1
        else: # Both are end points, find the one furthest away
            return 1 if dist_p < dist_q else -1

    return cmp_counterclockwise

# Use the compare function to create a sorted list of line segments
# sorted around a point p.
def sort_lines_counterclockwise(lines, p):
    points = []
    compare = cmp_counterclockwise_around(p)
    for line in lines:
        # If one end point comes before the other
        # add the points with the proper label to the queue
        if compare((line.p, line), (line.q, line)) < 0:
            points.append((line.p, line))
            points.append((line.q, line))
            line.start = line.p
        else:
            points.append((line.q, line))
            points.append((line.p, line))
            line.start = line.q
    srt = sorted(points, cmp=compare)
    return srt


####################
#   Random input   #
####################

import random

# Create a random point given a lower bound and an upper bound
def random_point(lb = 0, ub = 10000):
    return Point(random.randint(lb,ub), random.randint(lb,ub))

# Create n random points. Pass potential lb and ub to random_point.
def random_points(n = 100, *args, **kwargs):
    return [random_point(*args, **kwargs) for i in range(n)]

# Create random line segment by creating 2 random points.
# Pass potential lb and ub to random_point.
def random_lineseg(*args, **kwargs):
    return LineSeg(random_point(*args, **kwargs), random_point(*args, **kwargs))

# Create n random line segments.
# Pass potential lb and ub down to random_point.
def random_linesegs(n = 100, *args, **kwargs):
    return [random_lineseg(*args, **kwargs) for i in range(n)]

# Creates n random disjoint line segments
# Note: To keep this simple, naive line segment intersection
# has been used, thus the running time to generate this is O(n^2).
# This algorithm also tries for a fixed number of times to
# generate a line segment that does not intersect with other segments.
# If this is not possible, it will be dropped meaning that the number
# of actual line segments in the result is <= n (as opposed to == n).
# Pass potential lb and ub down to random_point.
def random_disjoint_linesegs(n = 100, tries = 10, *args, **kwargs):
    segs = []
    for i in range(n):
        no_intersections = True
        for t in range(tries):
            seg = random_lineseg(*args, **kwargs)
            for other_seg in segs:
                intersection = seg.intersects(other_seg)
                if intersection is not None:
                    no_intersections = False
                    break
            if no_intersections:
                segs.append(seg)
                break
    return segs


################
#   Plotting   #
################

# Use matplotlib to print points and line segments.
# Matplotlib takes a list of x coordinates and a list of y coordinates.
import matplotlib.pyplot as plt

def plot_points(l, show=False):
    if l == []:
        return

    # Split the x and y coordinates of the list of points
    # into separate lists. (Basically an "unzip" function)
    xs = []
    ys = []
    for p in l:
        xs.append(p.x)
        ys.append(p.y)

    plt.plot(xs, ys, 'o')

    if show:
        plt.show()

def plot_linesegs(l, color="", show=False):
    if l == []:
        return

    # Each line is drawn separately. 
    # For each line, create x and y coordinate lists
    # for the lines two end points.
    for line in l:
        if line is not None: # Edit here
            plt.plot([line.p.x, line.q.x], [line.p.y, line.q.y], color + '-')

    if show:
        plt.show()
