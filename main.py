#!/usr/bin/env python2

from geometry import *
from inputoutput import *
from eventhandler import *
from test import *

import argparse

parser = argparse.ArgumentParser(description='Compute the Voronoi diagram of a set of points')

parser.add_argument("-t", "--test", help="Run tests up to input size N", type=int,
                    nargs="?", metavar = "N", const=200)

parser.add_argument("-lb", "--lowerbound", help="The lower bound of possible random points and drawing canvas", type=int,
                    nargs="?", metavar = "N", default=0)
parser.add_argument("-ub", "--upperbound", help="The upper bound of possible random points and drawing canvas", type=int,
                    nargs="?", metavar = "N", default=100)

parser.add_argument("-d", "--debug", help="Enable debugging", action="store_true")
parser.add_argument("-c", "--convex", help="Show convex hull of input points", action="store_true")
parser.add_argument("-v", "--visualize", help="Enable visualization of each step", action="store_true")

parser.add_argument("-r", "--random", help="Generate a random input of size N and compute the Voronoi diagram of that", type=int,
                    nargs="?", metavar = "N", const=10)

parser.add_argument("-i", dest="input_filename",
                    help="File containing input", metavar="FILE")

parser.add_argument("-o", dest="output_filename",
                    help="File where the result will be written", metavar="FILE")

args = parser.parse_args()


if args.test:
    if args.output_filename:
        print "Running tests of input size up to %d...to file %s" % (args.test, args.output_filename)
        run_tests(args.test, 5, args.output_filename, args=args)
    else:
        print "Running tests of input size up to %d..." % args.test
        run_tests(args.test, args=args)
elif args.random or args.input_filename:
    if args.random: # Try with n random points
        # Generate a list of random points of size N
        sites = random_points(args.random, lb=args.lowerbound,ub=args.upperbound)

        print "Generated the following %d points:" % args.random
        print sites
    else: # We have an input file
        print "Loading file '%s'" % args.input_filename
        sites = input_from_file(args.input_filename)

    if not args.input_filename:
        args.input_filename = "random"

    if args.output_filename:
        args.output_file = args.output_filename
    else:
        args.output_file = args.input_filename + ".result"

    print "Performing Voronoi diagram algorithm"
    result = voronoi(sites, args)

    print "Outputting edges to file '%s'" % args.output_file
    output_to_file(args.output_file, result)
else:
    # Display help message
    parser.parse_args(["-h"])
