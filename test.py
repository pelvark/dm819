from geometry import *
from datetime import datetime
from eventhandler import *
import random

def test(n, args = None):
    c = 0
    i = 0
    while i < 5:
        # Input
        points = []
        numbers = random.sample(range(0,10*n),2*n)
        for i in range(0,n):
            points.append(Point(numbers[i],numbers[i+1]))

        # Timetaking
        a = datetime.now()
        
        try:
            result = voronoi(points, args)
            b = datetime.now()
            c = c + (b - a).total_seconds()
            i = i + 1
        except TypeError:
            print "=== None error! Printing relevant information ==="
            print p, segs
        
    
    result = c/5.0
    return n, result

def run_tests(n,i = 5, filename = 'testresults', args = None):
    results = []
    while i <= n:
        result = test(i, args)
        results.append(result)
        i = i + 5
    
    f = open(filename, 'w')
    f.write('\n'.join('%s %s' % x for x in results))

