from geometry import *

##################################
#   Doubly-connected edge list   #
##################################


class Half_edge(object):
    def __init__(self,
            origin_vertex=None,
            next_edge=None,
            prev_edge=None,
            twin=None,
            face=None,
            direction=None):
        #next and previous edges are edges in same face.
        self.next_edge = next_edge
        self.prev_edge = prev_edge
        #origin vertex is just the vertex the edge originates from
        self.origin_vertex = origin_vertex
        self.face = face
        self.twin = twin
        self.direction = direction

    def destination_vertex(self):
        if self.twin is not None:
            return self.twin.origin_vertex
        else:
            return None

    @staticmethod
    def create_twins(v1=None, v2=None):
        halfedge1 = Half_edge(origin_vertex=v1)
        halfedge2 = Half_edge(origin_vertex=v2)

        halfedge1.twin = halfedge2
        halfedge2.twin = halfedge1

        return (halfedge1, halfedge2)


class Vertex(object):
    def __init__(self, point, edge=None): # Use point from geometry
        self.point = point
        self.some_edge = edge # Any edge is sufficient, since all edges can be accessed from this using next/twin

    def __str__(self):
        return "%s" % self.point

    def __repr__(self):
        return str(self)

    def out_edges(self):
        l = []
        current = self.some_edge
        if current is not None:
            l.append(current)
        while current and current.twin and current.twin.next_edge is not self.some_edge:
            current = current.twin.next_edge
            if current is not None:
                l.append(current)

        return l

    def in_edges(self):
        return [e.twin for e in self.out_edges()]

    def reachable_vertices(self):
        return [e.destination_vertex() for e in self.out_edges()]


inf = float("inf")
infinite_vertex = Vertex(Point(inf, inf))


################
#   Plotting   #
################

def plot_vertex(v, color="", show=False):
    if v is infinite_vertex:
        print "Cannot plot inifinte vertex"
        return
    if hasattr(v, "drawn"):
        return
    if v is None:
        print "v is None"
        return

    all_vertices = v.reachable_vertices()
    vertices = [vertex for vertex in all_vertices if not hasattr(vertex, "drawn")]
    linesegs = []

    for other_v in vertices:
        if other_v is infinite_vertex:
            print "Cannot plot infinite vertex"
            continue
        if other_v is None:
            print "other_v is None"
            continue
        lineseg = LineSeg(v.point, other_v.point)
        linesegs.append(lineseg)

    plot_linesegs(linesegs, color)
    plot_points([v.point], color)

    v.drawn = True

    for next_vertices in vertices:
        plot_vertex(next_vertices, color)
