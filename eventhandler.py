from heap import *
from redblacktree import *
from copy import *
from dcel import *
from geometry import *

import math
import subprocess

epsilon = 0.0001

def voronoi(sites, args):
    # --- Initialization ---
    Q = Heap()
    for p in sites:
        heapnode = HeapNode(p, None, None)
        Q.max_heap_insert(heapnode)

    def compare(new_node, existing_node):
        guide_x = new_node.p.x

        interval = existing_node.interval(sweepline_y)

        if interval is None:
            print "No interval for arc %s" % (existing_node.p)
            return

        x1, x2 = interval

        if x1 <= guide_x <= x2:
            return 0
        elif guide_x <= x1:
            return -1
        else: # x2 <= guide_x
            return 1
    
    status = Rbt(compare_nodes=compare)
    dcel_edges = []
    
    sweepline_y = 0
    i = 1

    #handle events
    while not Q.is_empty():
        event = Q.heap_extract_max()
        sweepline_y = event.point.y - epsilon

        if event.center is None:
            handle_site_event(Q, status, event, sweepline_y, dcel_edges)
        else:
            if not event.skip:
                handle_circle_event(Q, status, event, sweepline_y, dcel_edges)

        if args.visualize:
            visualize_step(sites, sweepline_y, Q, status, dcel_edges, i, args)

        i += 1
    

    # Check to see whether the remaining redblacktree is erroneous
    if status.rbGetMin().left_edge is not None:
        print "wrongly have an edge left of left most arc"
    if status.rbGetMax().right_edge is not None:
        print "wrongly have an edge left of left most arc"
    if status.is_empty():
        print "status is empty"


    #####################################################
    # The remaining nodes in the redblacktree           #
    # represents the infinite halfedges.                #
    # These should be put into the dcel as well.        #
    # Possibly with some information about direction.   #
    #####################################################

    leftmost_arc  = status.rbGetMin()

    curr = leftmost_arc
    while curr.right_neighbor is not status.nil:
        right = curr.right_neighbor
        if curr.right_edge is None:
            print curr, "does not have a right edge"
            curr = curr.right_neighbor
            continue

        if curr.right_edge.origin_vertex is None:
            print curr, "right edge's origin vertex is none"
            curr = curr.right_neighbor
            continue

        # Find the midpoint between the two sites
        # and let the edge originating from the vertex
        # go through that point
        l = LineSeg(curr.site, right.site)
        if args.convex:
            plot_linesegs([l], "r")
        midpoint = l.midpoint()
        d = LineSeg(curr.right_edge.origin_vertex.point, midpoint).dir()

        flip = 1
        if Point.turn(curr.site, curr.right_edge.origin_vertex.point, right.site) == Point.LEFT_TURN:
            flip = -1

        curr.right_edge.direction = d * flip

        dcel_edges.append(curr.right_edge)

        # Go to the next node
        curr = curr.right_neighbor


    print "Outputting diagram as image to file '%s'" % (args.output_file + ".png")

    # Final visualization
    visualize_step(sites, args.lowerbound, Heap(), Rbt(), dcel_edges, -1, args)

    return dcel_edges

def handle_site_event(Q, status, event, sweepline_y, dcel_edges):
    if status.is_empty():
        # Simply insert first arc in status
        status.insert(event.point, event.point)
        return

    global epsilon

    #########################################
    # First we find the parabola above the  #
    # site event and split it into 2 arcs,  #
    # one on each side of the site event.   #
    #########################################

    split = status.find(event.point, event.point) # find the parabola that needs to be split
    # delete circle event in queue
    if split.circle_event is not None:
        #Q.heap_delete_key(split.circle_event)
        if split.circle_event.center is not None:
            split.circle_event.skip = True
        else:
            print "Circle event without center point"
        split.circle_event = None
        
    old_site = split.site
    split.site = event.point
    # Insert split node again just to the left of new arc, and add the halfedge from the old node
    status.insert(old_site, event.point.move(-epsilon, 0), True, left_edge=split.left_edge)
    # Insert split node again just to the right of new arc, and add the halfedge from the old node
    status.insert(old_site, event.point.move(+epsilon, 0), False, right_edge=split.right_edge)




    #############################################
    # Now we calculate possible circle events   #
    # and add them to the event queue.          #
    # both on left and right side of site event #
    #############################################

    # add circle event corresponding to the right side split of the old arc 
    if split.right_neighbor.right_neighbor is not status.nil:
        if Point.turn(event.point, old_site, split.right_neighbor.right_neighbor.site) == -1:
            circle = Circle.from_points(event.point, old_site, split.right_neighbor.right_neighbor.site)
            if circle is not None:
                bottom = circle.get_bottom()
                circle_event = HeapNode(bottom, split.right_neighbor, circle.center)
                Q.max_heap_insert(circle_event)
                split.right_neighbor.circle_event = circle_event
        

    # add circle event corresponding to the left side split of the old arc                        
    if split.left_neighbor.left_neighbor is not status.nil:                                         
        if Point.turn(split.left_neighbor.left_neighbor.site, old_site, event.point) == -1: 
            circle = Circle.from_points(split.left_neighbor.left_neighbor.site, old_site, event.point) 
            if circle is not None:                                                          
                bottom = circle.get_bottom() 
                circle_event = HeapNode(bottom, split.left_neighbor, circle.center)   
                Q.max_heap_insert(circle_event)                                
                split.left_neighbor.circle_event = circle_event                    


    #####################################
    # Create all new edges for          #
    # the new arc and the splitted      #
    # arcs two copies.                  #
    #####################################

    (new_edge1, new_edge2) = Half_edge.create_twins()

    if split.left_neighbor is status.nil:
        print "gosh darn"

    split.right_edge = new_edge1
    split.right_neighbor.left_edge = new_edge1

    split.left_edge = new_edge2
    split.left_neighbor.right_edge = new_edge2

    #bisec = LineSeg(old_site, event.point).bisector()
    #new_edge1.direction = -bisec.dir()
    #new_edge2.direction = bisec.dir()
            
    dcel_edges.append(new_edge1)



def handle_circle_event(Q, status, event, sweepline_y, dcel_edges):
    
    ##################################
    # We delete the arc from the     #
    # red black tree, and we also    #
    # delete any other circle events #
    # caused by this arc.            #
    ##################################

    if event.arc is not None:
        left = event.arc.left_neighbor
        right = event.arc.right_neighbor
        # Delete arc if circle center is above arc point.
        #if event.center.y < event.arc.site.y:
        status.rbDelete(event.arc)
        # Delete circle events involving deleted arc
        if left.circle_event is not None:
            #Q.heap_delete_key(left.circle_event)
            if left.circle_event.center is not None:
                left.circle_event.skip = True
            else:
                print "Circle event with missing center"
        if right.circle_event is not None:
            #Q.heap_delete_key(right.circle_event)
            if right.circle_event.center is not None:
                right.circle_event.skip = True
            else:
                print "Circle event with missing center"


        ######################################
        # We add a vertex to the DCEL,       #
        # and start a halfedge leaving       #
        # the vertex, (tracing the collision #
        # of beachlines by neighboring arcs) #
        ######################################


        if event.arc.left_neighbor is status.nil:
            print "Oh no, site event on leftmost arc"

        if event.arc.right_neighbor is status.nil:
            print "Oh no, site event on rightmost arc"

        lefte = event.arc.left_edge
        righte = event.arc.right_edge

        new_vertex = Vertex(event.center)
        (new_edge1, new_edge2) = Half_edge.create_twins()
        new_edge1.origin_vertex = new_vertex
        dcel_edges.append(new_edge1)

        # Ensure that neighbors' edges are set correctly
        left.right_edge = new_edge1
        right.left_edge = new_edge1

        # Update DCEL
        new_vertex.some_edge = lefte.twin
        lefte.twin.origin_vertex = new_vertex
        righte.twin.origin_vertex = new_vertex

        lefte.next_edge = righte.twin

        righte.twin.prev_edge = lefte
        righte.next_edge = new_edge1
        
        new_edge1.prev_edge = righte
        new_edge2.next_edge = lefte.twin

        lefte.twin.prev_edge = new_edge2


        #########################################
        # We check the removed arcs neighbor    #
        # and add any circle events where       #
        # where they would be removed.          #
        #########################################

        if left.left_neighbor is not status.nil:
            # circle event with left.left_neighbor, left, right
            if Point.turn(left.left_neighbor.site, left.site, right.site) == -1:
                circle = Circle.from_points(left.left_neighbor.site, left.site, right.site)
                if circle is not None:
                    bottom = circle.get_bottom()
                    circle_event = HeapNode(bottom, left, circle.center)
                    Q.max_heap_insert(circle_event)
                    left.circle_event = circle_event                                        


        if right.right_neighbor is not status.nil:
            # circle event with left, right, right.right_neighbor
            if Point.turn(left.site, right.site, right.right_neighbor.site) == -1:
                circle = Circle.from_points(left.site, right.site, right.right_neighbor.site)
                if circle is not None:
                    bottom = circle.get_bottom()
                    circle_event = HeapNode(circle.get_bottom(), right, circle.center)
                    Q.max_heap_insert(circle_event)
                    right.circle_event = circle_event                                        

    else: print "ERROR: There was no arc above circle event"

    return



def visualize_step(sites, sweepline_y, queue, status, dcel_edges, i, args):
    num_lead_zeros = int(math.log(2*len(sites))/math.log(10)) + 1

    lb = args.lowerbound
    ub = args.upperbound

    subprocess.call(["mkdir", "-p", "steps"])

    # Draw status in dot-format
    status_dot = status.toDot()
    dot_file = ("steps/status%0"+str(num_lead_zeros)+"d.dot") % i
    with open(dot_file, 'w') as f:
        f.write(status_dot)
    subprocess.call(["dot", "-Tpdf", dot_file, "-o", ("steps/status%0"+str(num_lead_zeros)+"d.pdf") % i])

    # Plot dcel edges
    for e in dcel_edges:
        orig = e.origin_vertex
        dest = e.destination_vertex()
        if orig:
            plot_points([orig.point], "k")
            if e.direction:
                l = LineSeg(orig.point, orig.point+e.direction*(ub+lb))
                plot_linesegs([l], "k")


        if dest:
            plot_points([dest.point], "k")

        if orig and dest:
            l = LineSeg(orig.point, dest.point)
            plot_linesegs([l], "k")



    # Plot site event points and circle event points
    circle_points = [e.point for e in queue.array[:-1] if e.point not in sites]

    plot_points(sites, "b")
    plot_points(circle_points, "r")

    # Plot parabolas
    parabolas = []
    status.inOrderTreeWalk(lambda node: parabolas.append(node.parabola(sweepline_y)))
    intervals = []
    status.inOrderTreeWalk(lambda node: intervals.append(node.interval(sweepline_y, lb=lb, ub=ub)))

    for parabola, interval in zip(parabolas, intervals):
        plot_parabola(parabola, interval)

    # Plot sweep line
    plt.plot((lb,ub), (sweepline_y, sweepline_y), 'b-')

    plt.xlim([lb,ub])
    plt.ylim([lb,ub])

    # If i is set to -1, it indicates that this is the final output
    if i == -1:
        plt.savefig(args.output_file + ".png")
    else:
        plt.savefig(("steps/plot%0"+str(num_lead_zeros)+"d.png") % i)
    plt.clf()

