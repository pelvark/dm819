import re
from geometry import *

def input_from_file(filename):
    re_num = r"([-+]?[0-9]*\.?[0-9]*)"
    re_point = r"(Point)?\(?%s,?\s*%s\)?" % (re_num, re_num)
    pattern = re.compile("%s" % re_point)
    # Read the file line by line and extract the sites
    with open(filename) as f:
        file_lines = f.readlines()
        sites = []
        for file_line in file_lines:
            # Search for site
            m = pattern.search(file_line)
            if m:
                px, py = m.group(2), m.group(3)
                if px and py:
                    site = Point(float(px), float(py))
                    sites.append(site)

        if sites == []:
            print "No sites given in file '%s'." % filename
            return None

        return sites

def output_to_file(filename, output):
    if output is None:
        return
    with open(filename, 'w+') as f:
        for edge in output:
            orig = edge.origin_vertex
            dest = edge.destination_vertex()

            fmt = "(%g, %g) (%g, %g)"
            if orig is not None and dest:
                line_str = fmt % (orig.point.x, orig.point.y, dest.point.x, dest.point.y)
                f.write("%s\n" % line_str)
            elif orig and edge.direction:
                l = LineSeg(orig.point, orig.point+edge.direction)
                line_str = (fmt + " (inf)") % (l.p.x, l.p.y, l.q.x, l.q.y)
                f.write("%s\n" % line_str)
            elif dest and edge.direction:
                l = LineSeg(dest.point, dest.point+edge.direction)
                line_str = (fmt + " (inf)") % (l.p.x, l.p.y, l.q.x, l.q.y)
                f.write("%s\n" % line_str)
