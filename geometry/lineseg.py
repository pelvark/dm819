import math
import random
from point import *

#########################################
#   Geometric data structures/classes   #
#########################################

# Line segment defined by left endpoint, p, and right endpoint, q.
# Supports finding intersection with other line segment
class LineSeg:
    def __init__(self, p, q, infinite = False):
        self.p = p
        self.q = q
        self.is_removed = False
        self.start = None
        self.infinite = infinite

    def __cmp__(self, l):
        return cmp(self.to_tuple(), l.to_tuple())

    def __str__(self):
        #return "%s - %s" % (self.p, self.q)
        return self.__repr__()

    def __repr__(self):
        return "LineSeg(%s, %s)" % (self.p, self.q)

    def dir(self):
        return (self.q - self.p)

    def to_tuple(self):
        return (self.p, self.q)

    def other_end(self, point):
        if point == self.p:
            return self.q
        elif point == self.q:
            return self.p
        else:
            print "Something is wrong! %s is neither end points of %s" % (point, self)

    def midpoint(self):
        return (self.p + self.q) / 2

    def normal(self):
        return self.dir().flip().normalize()

    def bisector(self):
        mid = self.midpoint()
        normal = self.normal()
        return LineSeg(mid, mid + normal, infinite=True)

    def parallel_with(self, other, epsilon=0.00001):
        dir1 = self.dir()
        dir2 = other.dir()
        a1 = dir1.signed_angle_between(dir2)
        a2 = dir1.signed_angle_between(-dir2)
        return a1 < epsilon or a2 < epsilon

    # Check if point is on the line.
    def on_line(self, c, epsilon=0.0001):
        return abs((self.p.distance_to(c) + self.q.distance_to(c)) - self.p.distance_to(self.q)) < epsilon

    # Find intersection between lines
    def intersects(self, l):
        if not isinstance(l, LineSeg):
            print "Intersection betweeen line and %s is not supported" % type(other)
            return

        if self.infinite and l.infinite:
            x1, y1 = self.p.to_tuple()
            x2, y2 = self.q.to_tuple()
            x3, y3 =    l.p.to_tuple()
            x4, y4 =    l.q.to_tuple()
            denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
            if denominator == 0:
                # Parallel lines
                if   self.on_line(l.p): return l.p
                elif self.on_line(l.q): return l.q
                elif l.on_line(self.p): return self.p
                elif l.on_line(self.q): return self.q
                else: return None # Disjoint lines
            x_numerator = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)
            y_numerator = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)
            x = x_numerator / float(denominator)
            y = y_numerator / float(denominator)
            return Point(x, y)
        elif self.infinite and not l.infinite:
            # Find the intersection as if both lines are infinite
            l.infinite = True
            intersection = self.intersects(l)
            l.infinite = False
            if intersection is None:
                return None
            # Check if the intersection is on the line segment
            if l.on_line(intersection):
                return intersection
            else:
                return None
        elif not self.infinite and l.infinite:
            return l.intersects(self)
        else: # not self.infinite and not l.infinite
            (p,r) = (self.p, self.dir())
            (q,s) = (l.p, l.dir())
            d = r.det(s)
            if d == 0:
                k = (q-p).det(r)
                if k == 0: # Colinear
                    # Check for overlapping interval here
                    return None
                else:
                    return None
            else:
                t = (q-p).det(s/d)
                u = (q-p).det(r/d)
                if 0 <= t <= 1 and 0 <= u <= 1:
                    return p + r * t
                else:
                    return None


####################
#   Random input   #
####################

# Create random line segment by creating 2 random points.
# Pass potential lb and ub to random_point.
def random_lineseg(*args, **kwargs):
    return LineSeg(random_point(*args, **kwargs), random_point(*args, **kwargs))

# Create n random line segments.
# Pass potential lb and ub down to random_point.
def random_linesegs(n = 100, *args, **kwargs):
    return [random_lineseg(*args, **kwargs) for i in range(n)]


################
#   Plotting   #
################

# Use matplotlib to print points and line segments.
# Matplotlib takes a list of x coordinates and a list of y coordinates.
import matplotlib.pyplot as plt

def plot_linesegs(l, color="", show=False):
    if l == []:
        return

    # Each line is drawn separately. 
    # For each line, create x and y coordinate lists
    # for the lines two end points.
    for line in l:
        if line is not None:
            plt.plot([line.p.x, line.q.x], [line.p.y, line.q.y], color + '-')

    if show:
        plt.show()
