import math
from point import *
from lineseg import *

#########################################
#   Geometric data structures/classes   #
#########################################

class Parabola:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    @staticmethod
    def from_site_and_sweepline(site, sweepline_y):
        if site.y == sweepline_y:
            print "Parabola of infinite slope"
        coeff = 1./(2*(site.y - sweepline_y))
        a = coeff
        b = -2*site.x * coeff
        c = (site.x**2 + site.y**2 - sweepline_y**2) * coeff
        return Parabola(a, b, c)

    def to_tuple(self):
        return (self.a, self.b, self.c)

    def equals(self, other):
        return self.a == other.a and self.b == other.b and self.c == other.c

    def __repr__(self):
        return "Parabola(%s, %s, %s)" % (self.a, self.b, self.c)

    def __str__(self):
        return self.formula()

    def formula(self):
        return "%g x^2 + %g x + %g" % (self.a, self.b, self.c)

    def determinant(self):
        return self.b**2 - 4 * self.a * self.c

    def evaluate(self, x):
        return self.a * x**2 + self.b * x + self.c

    # The "top point" of the parabola, called the vertex of the parabola in English
    def vertex(self):
        x = - self.b / (2 * self.a)
        y = - self.determinant() / (4 * self.a)
        return Point(x, y)

    def solutions(self):
        a, b, c = self.a, self.b, self.c
        d = self.determinant()

        if d < 0:
            return None
        elif d == 0:
            return -b / (2 * a)
        else: # d > 0
            x1 = (-b + math.sqrt(d)) / (2*a)
            x2 = (-b - math.sqrt(d)) / (2*a)
            return (x1, x2)

    def intersects(self, other):
        if isinstance(other, Parabola):
            # Consider the two parabolas
            # a x^2 + b x + c
            # d x^2 + e x + f
            a, b, c = self.a, self.b, self.c
            d, e, f = other.a, other.b, other.c

            # The intersection of the two parabolas
            # corresponds to finding the solution to
            # (a-d) x^2 + (b-e) x + (c-f) = 0

            # But if a - d = 0, there is only one solution
            if a - d == 0:
                # And if b - e = 0, there is none
                if b - e == 0:
                    return None
                solution = (f - c) / (b - e)
                p = Point(solution, self.evaluate(solution))
                return p

            new_para = Parabola(a - d, b - e, c - f)
            solutions = new_para.solutions()
            if solutions is None:
                #print "No solution to %s, which is based on %s and %s" % (new_para, self, other)
                return None
            p0 = Point(solutions[0], self.evaluate(solutions[0]))
            p1 = Point(solutions[1], self.evaluate(solutions[1]))
            return (p0, p1)
        else:
            print "Cannot find intersection between parabola and %s %s" % (other, type(other))


################
#   Plotting   #
################

# Use matplotlib to print points and line segments.
# Matplotlib takes a list of x coordinates and a list of y coordinates.
import matplotlib.pyplot as plt
import numpy as np

def plot_parabola(para, interval=(0,100), resolution=1000, color="k", show=False):
    if para is None:
        return

    # Short-hands
    a, b, c = para.a, para.b, para.c

    # Points to create continuous curve
    x = np.linspace(interval[0], interval[1], resolution)

    # Formula of parabola
    y = a * x**2 + b * x + c

    plt.plot(x, y, color + "-")

    if show:
        plt.show()
