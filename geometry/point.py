import math

#########################################
#   Geometric data structures/classes   #
#########################################

# Point in 2D
# Implements operations on points, such as +,-,*,/ etc
# as well as distance, dot product and determinant/cross product
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, p):
        return Point(self.x + p.x, self.y + p.y)

    def __sub__(self, p):
        return self + (-p)

    def __mul__(self, t):
        return Point(self.x * t, self.y * t)

    def __div__(self, t):
        return Point(self.x / float(t), self.y / float(t))

    def __pos__(self):
        return self.clone()

    def __neg__(self):
        return self * (-1)

    def __cmp__(self, p):
        return cmp(self.to_tuple(), p.to_tuple())

    def __str__(self):
        #return "(%s, %s)" % (self.x, self.y)
        return self.__repr__()

    def __repr__(self):
        return "Point(%s, %s)" % (self.x, self.y)

    def to_tuple(self):
        return (self.x, self.y)

    def clone(self):
        return Point(self.x, self.y)

    def length(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def move(self, dx, dy):
        return self + Point(dx, dy)

    def normalize(self):
        return self / self.length()

    def distance_to(self, p):
        return (self - p).length()

    def signed_angle_between(self, p):
        # Based on http://stackoverflow.com/a/2150475
        a = math.atan2(self.det(p), self.dot(p))
        return a if a >= 0 else a + 2*math.pi

    def dot(self, p):
        return self.x * p.x + self.y * p.y

    def det(self, p):
        return self.x * p.y - self.y * p.x

    def flip(self):
        return Point(-self.y, self.x)

    # Define constants for better readability
    LEFT_TURN, RIGHT_TURN, COLINEAR = (1, -1, 0)

    # Returns 1, -1, 0 if p,q,r forms a left turn, a right turn or are colinear.
    @staticmethod
    def turn(p, q, r):
        return cmp(p.det(q) + r.det(p) + q.det(r), 0)


####################
#   Random input   #
####################

import random

# Create a random point given a lower bound and an upper bound
def random_point(lb = 0, ub = 100):
    return Point(random.uniform(lb,ub), random.uniform(lb,ub))

# Create n random points. Pass potential lb and ub to random_point.
def random_points(n = 100, *args, **kwargs):
    return [random_point(*args, **kwargs) for i in range(n)]


################
#   Plotting   #
################

# Use matplotlib to print points and line segments.
# Matplotlib takes a list of x coordinates and a list of y coordinates.
import matplotlib.pyplot as plt

def plot_points(l, color="", show=False):
    if l == []:
        return

    # Split the x and y coordinates of the list of points
    # into separate lists. (Basically an "unzip" function)
    xs = []
    ys = []
    for p in l:
        if p is not None:
            xs.append(p.x)
            ys.append(p.y)

    plt.plot(xs, ys, color + 'o')

    if show:
        plt.show()
