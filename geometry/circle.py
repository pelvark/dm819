import math
from point import *
from lineseg import *

#########################################
#   Geometric data structures/classes   #
#########################################

class Circle:
    def __init__(self, center, radius):
        self.center = center
        self.radius = radius

    def get_bottom(self):   # get lowest point on circle
        bot_y = self.center.y - self.radius 
        return Point(self.center.x, bot_y)

    @staticmethod
    def from_points(p, q, r):
        # We can't construct a circle of using only effectively 2 points
        if p == q or p == r or q == r:
            print "Error: One of p (%s), q (%s) and r (%s) are equal" % (p, q, r)
            return None

        if Point.turn(p,q,r) == Point.COLINEAR:
            raise ValueError("Points %s %s %s are colinear. This is not general position")

        # Connect the points in a triangle (we don't need the last side)
        l_pq = LineSeg(p, q)
        l_pr = LineSeg(p, r)

        # Find the bisectors of the sides
        bisector_pq = l_pq.bisector()
        bisector_pr = l_pr.bisector()

        # All the bisectors will meet in a common point,
        # which will be the center of the circumcircle
        intersection = bisector_pq.intersects(bisector_pr)

        if intersection is None:
            print "Points %s, %s, %s are colinear and does not have a circle in common" % (p,q,r)
            return

        # The radius of the circumcircle is the distance to any of the points, for instance p
        radius = intersection.distance_to(p)

        return Circle(intersection, radius)


################
#   Plotting   #
################

# Use matplotlib to print points and line segments.
# Matplotlib takes a list of x coordinates and a list of y coordinates.
import matplotlib.pyplot as plt

def plot_circle(circ, color="k", fill=False, show=False):
    if circ is None:
        return

    plt_circle = plt.Circle(circ.center.to_tuple(), circ.radius, color=color, fill=fill)
    plt.gcf().gca().add_artist(plt_circle)

    if show:
        plt.show()
