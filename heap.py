from geometry import *

class HeapNode(object):
    def __init__(self, p, pointer, center):
        self.point = p
        self.priority = p.y
        self.position = -1
        self.center = center # center point of a circle, none if site event 

        self.skip = False

        # pointer
        self.arc = pointer # pointer to a circle events corresponding arc in rbt
    




class Heap(object):
    def __init__(self):
        self.size = 0
        #tiny must always be smaller than any other element in list. make infinite at some point.
        self.tiny = HeapNode(Point(0, -float("inf")), None, None)
        self.array = [self.tiny]

    def is_empty(self):
        return self.size == 0

    def heap_extract_max(self):
        if self.size < 1:
            print "trying to extract max from empty heap"
            return
        maximum = self.array[0]
        #reduce size first because of 0-indexing
        self.size = self.size -1
        self.array[0] = self.array[self.size]
        self.array.pop()
        self.max_Heapify(0)
        return maximum

    # heapify pushes a key down until its position in the heap is correct
    def max_Heapify(self, i):
        l = left(i)
        r = right(i)
        if l < self.size and self.array[l].priority > self.array[i].priority:
            largest = l
        else: 
            largest = i
        if r < self.size and self.array[r].priority > self.array[largest].priority:
            largest = r
        if largest != i:
            self.array[self.size] = self.array[largest]
            self.array[largest] = self.array[i]
            self.array[i] = self.array[self.size]
            self.array[largest].position = largest
            self.array[i].position = i
            self.max_Heapify(largest)


    def insert(self, p, pointer=None, center=None):
        h = HeapNode(p, pointer, center)
        self.max_heap_insert(h)

    def max_heap_insert(self, key):
        self.array.append(self.tiny)
        self.size = self.size + 1
        self.heap_decrease_key(self.size-1, key)

    # decrease key swaps a node with it's parent until its place in the heap is correct
    def heap_decrease_key(self, i, key):
        if key.priority < self.tiny.priority:
            print "inserted key has smaller priority than the theoretical min"
            return
        self.array[i] = key    

        while i > 0 and self.array[parent(i)].priority < self.array[i].priority:
            p = parent(i)
            self.array[self.size] = self.array[p]
            self.array[p] = self.array[i]
            self.array[i] = self.array[self.size]
            
            self.array[i].position = i
            self.array[p].position = p
            i = p
    

    def heap_delete_key(self, key):
        i = key.position
        self.array[i] = self.array[self.size-1]
        self.array[i].position = i
        self.size = self.size-1
        self.array.pop()
        p = parent(i)
        if key.priority > self.array[p].priority:
            # must move up in heap
            self.heap_decrease_key(i, self.array[i])
            return
        elif key.priority < self.array[p].priority:
            # must move down in heap
            self.max_Heapify(i)
            return
        


#in 0-indexed.
def parent(i):
    return ((i+1)/2)-1
def left(i):
    return 2*i+1
def right(i):
    return 2*i+2





